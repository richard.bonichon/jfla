
const aviewers = Array.prototype.slice.call(document.querySelectorAll(".viewer"));

aviewers.forEach(
  (a, _) =>
    a.addEventListener(
      'click', 
      () => {
        const resume = a.nextElementSibling.nextElementSibling.nextElementSibling;
        const style = getComputedStyle(resume);
        // for some reason resume.style.display is initially empty "", though set in CSS file to none
        if (style.display == "block") {
          resume.style.display = "none";
          a.innerHTML = '<i aria-hidden="true" title="Voir le résumé" class="fas fa-2x fa-angle-right"></i>';
        }
        else if (style.display == "none") {
          resume.style.display = "block";                    
          a.innerHTML = '<i aria-hidden="true" title="Cacher le résumé" class="fas fa-2x fa-angle-up"></i>'; }
            })
);


