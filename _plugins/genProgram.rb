# coding: utf-8
module Jekyll

  module NameFilter
    def initials(s)
      def is_letter?(char)
        char.match(/^[[:alpha:]]$/)
      end

      abbrev = ""
      may_start = true

      s.each_char { |c|
        if is_letter?(c) then
          if may_start then
            abbrev += c + "."
          end
          may_start = false
        else
          may_start = true
          if c != '.' || may_start then
            abbrev += c
          end
        end
      }
      
      abbrev
    end

  end

  module TimeFilter
   def view_time(s_f)
      s = s_f.to_i
      hr = s / 60
      min = s % 60
      '%02d:%02d' % [hr, min]
    end
  end

  module ScheduleFilter
    def calcul_programme(d)
      puts "Génération du programme JFLA #{d["edition"]} ..."
      cal = d["planning"]
      talks = d["programme"]
      p = {}
      # puts "--"
      # puts "#{cal}"
      # puts "--"
      # puts "#{talks}"
      # puts "--"
      talks.each { |t|
        # puts "talk #{t}"
        slots = t["slots"]
        if slots then
          slots.each { |s| 
            v = t.clone # Make a copy to avoid side effects
            if s["debut"] and s["fin"]
              # L'heure a été spécifiée sans alias 
              v['heure'] = { "debut" => s['debut'], 
                             "fin"   => s["fin"] }
            else
              # on va chercher un alias
              v["heure"] = cal["horaires"][s["heure"]]
            end
            
            may_j = s["jour"]
            j = cal['jours'][may_j]
            if j then 
              v['jdate'] = j
            else
              v['jdate'] = may_j
            end
            j = v['jdate']
            #puts "Jour = #{j} #{v}"
            if p[j]
              p[j].push(v)
            else
              p[j] = [v]
            end
#            puts "#{p}"
            #puts "--"
          }
        end
        }

      # puts "Program = #{p}"
      # Tri par data/heure de début
      # Hypothèse: les heures ne se chevauche pas
      # En bref, on fait confiance à celui qui a entré les données
      pfinal = []
      size = 0
      # Vérifie que toutes les activités sont correctement renseignées
      # Sinon, lève une erreur plus claire qu'un NilClass
      p.each { |_, activities|
        activities.each { |a|
          if not (a['heure'] and a['heure']['debut'])
            raise "Impossiblité de générer le programme.Il manque le champ ['heure'] ou ['heure']['debut']\n#{a}\nRegardez si vous avez utitilisé un alias heure erroné dans le fichier de configuration."
          end
        }
      }
      p.each { |day, activities|
        # puts "\n\n>> #{day} \n #{activities}"
        a = activities.sort_by { |a| a['heure']['debut'] }
        size += a.size
        pfinal.push([day, a])
      }
      res = {}
      res["programme"] = pfinal.sort_by { | d | d[0] }#.map(d => {"journee" => d[0], "emploidutemps" => d[1]})
      res["size"] = size 
      res 
    end
  end
end

Liquid::Template.register_filter(Jekyll::ScheduleFilter)
Liquid::Template.register_filter(Jekyll::TimeFilter)
Liquid::Template.register_filter(Jekyll::NameFilter)

