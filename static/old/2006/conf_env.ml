(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2004 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

open Conf_env_president;;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)

(* Fields to fill to generate the Web site. *)
let nom_conf = "jfla";;
let upper_nom_conf = "JFLA";;

(* Fin des donn�es d�pendantes de la conf�rence. *)

let montant_chambre_simple = Conf_env_president.montant_chambre_simple;;
let montant_chambre_double = Conf_env_president.montant_chambre_double;;

let date_depart_conf = deuxieme_jour_conf;;

(* Administration des inscriptions. *)
let contact_administratif = "Marie-Fran�oise Loubressac";;
let mail_contact_administratif = "Marie-Francoise.Loubressac@inria.fr";;
let telephone_administratif =
  "T�l.: +33 (0) 1 39 63 56 00 - Fax : +33 (0) 1 39 63 56 38";;
let mail_reception_inscriptions = "symposia@inria.fr";;

(* Emplacement des sites Webs. *)

let http_root = "http://jfla.inria.fr";;
let http_conf_site_root = http_root;;

let info_touristiques = "\
<UL>
  <LI><A HREF=\"" ^ web_hotel_conf ^ "\"></A>
    ou <A HREF=\"http://www.tourisme.fr/tourist-office/pauillac.htm\">Office du tourisme de $(LIEU_CONF)</A>.
</UL>\
";;

let conf_call_back = "treat_form";;

let last_year_conf = year_conf - 1;;
let next_year_conf = year_conf + 1;;

let year_conf_str = string_of_int year_conf;;

let env = [
  "NOM_CONF", nom_conf;
  "UPPER_NOM_CONF", upper_nom_conf;

  "YEAR_CONF", year_conf_str;
  "THIS_YEAR", string_of_int this_year;
  "LAST_YEAR_CONF", string_of_int last_year_conf;
  "NEXT_YEAR_CONF", string_of_int next_year_conf;
  "NIEME_CONF", nieme_conf;
  "DATE_ARRIVEE_CONF", date_arrivee_conf;
  "DATE_DEPART_CONF", date_depart_conf;
  "PREMIER_JOUR_CONF", premier_jour_conf;
  "DEUXIEME_JOUR_CONF", deuxieme_jour_conf;
  "DATE_CONF", date_conf;

  "DATE_ARRIVEE_COURS", date_arrivee_cours;
  "PREMIER_JOUR_COURS", premier_jour_cours;
  "DEUXIEME_JOUR_CONF", deuxieme_jour_cours;

  "DATE_LIMITE_SOUMISSION", date_limite_soumission;
  "DATE_NOTIFICATION", date_notification;
  "DATE_REMISE_ARTICLE", date_remise_article;
  "DATE_LIMITE_INSCRIPTION", date_limite_inscription;

  "PRESIDENT_COMITE", president_comite;
  "MAIL_PRESIDENT_COMITE", mail_president_comite;
  "MAIL_SOUMISSION", mail_soumission;
  "INSTITUTION_PRESIDENT_COMITE", institution_president_comite;

  "MEMBRE_COMITE1", membre_comite1;
  "MAIL_MEMBRE_COMITE1", mail_membre_comite1;
  "INSTITUTION_MEMBRE_COMITE1", institution_membre_comite1;

  "MEMBRE_COMITE2", membre_comite2;
  "MAIL_MEMBRE_COMITE2", mail_membre_comite2;
  "INSTITUTION_MEMBRE_COMITE2", institution_membre_comite2;

  "MEMBRE_COMITE3", membre_comite3;
  "MAIL_MEMBRE_COMITE3", mail_membre_comite3;
  "INSTITUTION_MEMBRE_COMITE3", institution_membre_comite3;

  "MEMBRE_COMITE4", membre_comite4;
  "MAIL_MEMBRE_COMITE4", mail_membre_comite4;
  "INSTITUTION_MEMBRE_COMITE4", institution_membre_comite4;

  "MEMBRE_COMITE5", membre_comite5;
  "MAIL_MEMBRE_COMITE5", mail_membre_comite5;
  "INSTITUTION_MEMBRE_COMITE5", institution_membre_comite5;

  "MEMBRE_COMITE6", membre_comite6;
  "MAIL_MEMBRE_COMITE6", mail_membre_comite6;
  "INSTITUTION_MEMBRE_COMITE6", institution_membre_comite6;

  "MEMBRE_COMITE7", membre_comite7;
  "MAIL_MEMBRE_COMITE7", mail_membre_comite7;
  "INSTITUTION_MEMBRE_COMITE7", institution_membre_comite7;

  "MEMBRE_COMITE8", membre_comite8;
  "MAIL_MEMBRE_COMITE8", mail_membre_comite8;
  "INSTITUTION_MEMBRE_COMITE8", institution_membre_comite8;

  "MEMBRE_COMITE9", membre_comite9;
  "MAIL_MEMBRE_COMITE9", mail_membre_comite9;
  "INSTITUTION_MEMBRE_COMITE9", institution_membre_comite9;

  "MEMBRE_COMITE10", membre_comite10;
  "MAIL_MEMBRE_COMITE10", mail_membre_comite10;
  "INSTITUTION_MEMBRE_COMITE10", institution_membre_comite10;

  "ORATEUR_INVITE_1", orateur_invite_1;
  "INSTITUTION_ORATEUR_INVITE_1", institution_orateur_invite_1;
  "TITRE_CONF_INVITE_1", titre_conf_invite_1;
  "ORATEUR_INVITE_2", orateur_invite_2;
  "INSTITUTION_ORATEUR_INVITE_2", institution_orateur_invite_2;
  "TITRE_CONF_INVITE_2", titre_conf_invite_2;

  "PRESENTATION_1", presentation_1;
  "AUTEURS_1", auteurs_1;
  "PRESENTATION_2", presentation_2;
  "AUTEURS_2", auteurs_2;
  "PRESENTATION_3", presentation_3;
  "AUTEURS_3", auteurs_3;
  "PRESENTATION_4", presentation_4;
  "AUTEURS_4", auteurs_4;
  "PRESENTATION_5", presentation_5;
  "AUTEURS_5", auteurs_5;
  "PRESENTATION_6", presentation_6;
  "AUTEURS_6", auteurs_6;
  "PRESENTATION_7", presentation_7;
  "AUTEURS_7", auteurs_7;
  "PRESENTATION_8", presentation_8;
  "AUTEURS_8", auteurs_8;
  "PRESENTATION_9", presentation_9;
  "AUTEURS_9", auteurs_9;
  "PRESENTATION_10", presentation_10;
  "AUTEURS_10", auteurs_10;
  "PRESENTATION_11", presentation_11;
  "AUTEURS_11", auteurs_11;
  "PRESENTATION_12", presentation_12;
  "AUTEURS_12", auteurs_12;
  "TITRE_TABLE_RONDE", titre_table_ronde;

  "CONTACT_ADMINISTRATIF", contact_administratif;
  "MAIL_CONTACT_ADMINISTRATIF", mail_contact_administratif;
  "TELEPHONE_ADMINISTRATIF", telephone_administratif;
  "OU_CONF", ou_conf;
  "LIEU_CONF", lieu_conf;
  "GRANDE_VILLE_PROCHE_CONF", grande_ville_proche_conf;
  "DISTANCE_DE_BORDEAUX", distance_de_bordeaux;
  "DISTANCE_DE_LYON", distance_de_lyon;
  "DISTANCE_DE_MARSEILLE", distance_de_marseille;
  "DISTANCE_DE_NANTES", distance_de_nantes;
  "DISTANCE_DE_PARIS", distance_de_paris;
  "APPROCHE_CONF", approche_conf;
  "GARE_CONF", gare_conf;
  "AEROPORT_CONF", aeroport_conf;
  "DISTANCE_AEROPORT_CONF", distance_aeroport_conf;
  "AUTRE_AEROPORT_CONF", autre_aeroport_conf;
  "GARE_PARIS", gare_paris;
  "AEROPORT_PARIS", aeroport_paris;
  "HORAIRES_VOL_ALLER_PARIS", horaires_vol_aller_paris;
  "HORAIRES_VOL_RETOUR_PARIS", horaires_vol_retour_paris;
  "HORAIRES_TRAIN_ALLER_PARIS", horaires_train_aller_paris;
  "HORAIRES_TRAIN_RETOUR_PARIS", horaires_train_retour_paris;
  "GARE_NAVETTE", gare_navette;
  "HEURE_NAVETTE_ARRIVEE", heure_navette_arrivee;
  "HEURE_NAVETTE_DEPART", heure_navette_depart;
  "HOTEL_CONF", hotel_conf;
  "ADRESSE_HOTEL_CONF", adresse_hotel_conf;
  "WEB_HOTEL_CONF", web_hotel_conf;
  "PUB_SITE_CONF", pub_site_conf;

  "MONTANT_CHAMBRE_SIMPLE", montant_chambre_simple;
  "MONTANT_CHAMBRE_DOUBLE", montant_chambre_double;
  "MONTANT_DEVISE", montant_devise;

  "INFO_TOURISTIQUES", info_touristiques;
  "SOCIAL_EVENT_CONF", social_event_conf;
  "DATE_SOCIAL_EVENT_CONF", date_social_event_conf;

  "HTTP_ROOT", http_root;
  "HTTP_CONF_SITE_ROOT", http_conf_site_root;
  "cgi",
    ("http:///cgi-bin/inscription_jfla" ^ year_conf_str);
  "DataDirectory",
    ("/home/yquem/cristal/weis/Strass/CIME/jfla" ^
     year_conf_str ^ "/Data/");
  "call_back", conf_call_back;
 ];;

let year_conf = year_conf_str;;
