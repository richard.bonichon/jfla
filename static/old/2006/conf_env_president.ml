(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2004 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

(* The year when we are compiling this program! *)
let this_year = 2004;;
(* The year when the conference will hold. *)
let year_conf = 2005;;

let nieme_conf = "seizi�me";;

(* Dead-lines. *)
let date_limite_soumission = "4 octobre";;
let date_notification = "15 novembre";;
let date_remise_article = "09 d�cembre";;
let date_limite_inscription = "15 janvier";;

(* Dates of the meeting. *)
let date_conf = "31 janvier et 1 f�vrier";;
let premier_jour_conf = "31 janvier";;
let deuxieme_jour_conf = "1 f�vrier";;
let date_arrivee_conf = "dimanche 30 janvier";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "Olivier Michel";;
let mail_president_comite = "michel@lami.univ-evry.fr";;
let institution_president_comite = "LAMI, Universit� �vry-Val d'Essonne, �vry";;

(* Lee membres du comit� de programme. *)
let membre_comite1 = "Inconnu";;
let mail_membre_comite1 = "";;
let institution_membre_comite1 = "LSR, INPG";;

let membre_comite2 = "Inconnu";;
let mail_membre_comite2 = "";;
let institution_membre_comite2 = "LIP6, Paris";;

let membre_comite3 = "Inconnu";;
let mail_membre_comite3 = "";;
let institution_membre_comite3 = "OGI, USA";;

let membre_comite4 = "Inconnu";;
let mail_membre_comite4 = "";;
let institution_membre_comite4 = "LITP, Paris7";;

let membre_comite5 = "Inconnu";;
let mail_membre_comite5 = "";;
let institution_membre_comite5 = "INRIA, Rocquencourt";;

let membre_comite6 = "Inconnu";;
let mail_membre_comite6 = "";;
let institution_membre_comite6 = "LAMI, Universit� �vry-Val d'Essonne";;

let membre_comite7 = "Inconnu";;
let mail_membre_comite7 = "";;
let institution_membre_comite7 = "LIMA, ENSEEIHT";;

let membre_comite8 = "Inconnu";;
let mail_membre_comite8 = "";;
let institution_membre_comite8 = "INRIA, Sophia-Antipolis";;

let membre_comite9 = "Inconnu";;
let mail_membre_comite9 = "";;
let institution_membre_comite9 = "LORIA, Lorraine";;

(* Les conf�renciers invit�s. *)
let orateur_invite_1 = "Inconnu";;
let institution_orateur_invite_1 = "LSV, Ens Cachan";;
let titre_conf_invite_1 =
  "Une fois qu'on a trouv� la preuve, \
   comment le faire comprendre � un assistant de preuve?";;

let orateur_invite_2 = "Inconnu";;
let institution_orateur_invite_2 = "INRIA Rocquencourt";;
let titre_conf_invite_2 =
  "(Non communiqu�)";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
let ou_conf = "� la montagne";;
let lieu_conf = "Sainte-Marie-de-R� (Charente-Maritime)";;
let gare_conf = "La Rochelle";;
let aeroport_conf = "La Rochelle - �le de R�";;
let distance_aeroport_conf = "17km";;
let autre_aeroport_conf = "";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "Montparnasse";;
let horaires_train_aller_paris = "d�part 13h55, arriv�e � 17h17";;
let horaires_train_retour_paris = "d�part 17h12, arriv�e � 20h20";;
let aeroport_paris = "Orly Ouest";;
let distance_de_bordeaux = "190";;
let distance_de_lyon = "650";;
let distance_de_marseille = "824";;
let distance_de_nantes = "140";;
let distance_de_paris = "470";;
let distance_de_poitiers = "150";;
let approche_conf = "
(Non Communiqu�)
";;

(* Navette et h�tel. *)
let heure_navette_arrivee = "17h30";;
let heure_navette_depart = "15h30";;
let hotel_conf = "� Les Grenettes �";;
let web_hotel_conf = "http://www.hotel-les-grenettes.com/";;
let adresse_hotel_conf =
  printf__sprintf "\
<BLOCKQUOTE>
%s<BR>
route du Bois-Plage 17740 Sainte-Marie-de-R�<BR>
Tel: 05 46 30 22 47<BR>
Fax: 05 46 30 24 64<BR>
%s
</BLOCKQUOTE>
" hotel_conf web_hotel_conf;;

let pub_site_conf =
  "<IMG SRC=\"pub_site_conf.jpg\" ALT=\"Site de la manifestation\">";;

let montant_chambre_simple = "230";;
let montant_chambre_double = "190";;
let montant_devise = "euros";;

let social_event_conf = "une balade � pied ou en v�lo";;
let date_social_event_conf = "lundi " ^ premier_jour_conf ^ " apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence. *)
let presentation_1 = "Ocaml-templates, g�n�ration de code � partir des types";;
let auteurs_1 = "Fran�ois Maurel (PPS, Paris 7)";;
                                                                               
let presentation_2 =
  "Typage fort et typage souple des collections topologiques";;
let auteurs_2 = "Julien Cohen (LAMI, �vry)";;
 
let presentation_3 =
  "Une biblioth�que certifi�e de programmes fonctionnels BSP";;
let auteurs_3 = "Fr�d�ric Gava (LACL, Paris 12)";;
 
let presentation_4 =
  "Une proc�dure de d�cision r�flexive pour un fragment \
   de l'arithm�tique de Presburger";;
let auteurs_4 = "Pierre Cr�gut (France-T�l�com R & D - DTL/TAL)";;

let presentation_5 = "Gb: une proc�dure de d�cision pour le syst�me Coq";;
let auteurs_5 =
  "J�r�me Cr�ci (Saarbrucken), Lo�c Pottier (INRIA Sophia-Antipolis)";;
 
let presentation_6 = "Application du toplevel embarqu� d'Objective Caml";;
let auteurs_6 =
  "Cl�ment Capel, Emmanuel Chailloux (PPS, Paris 7), \
   Jean-Marc Eber (LexiFi SAS)";;
 
let presentation_7 = "GlSurf: maths et dessins en Ocaml";;
let auteurs_7 =
  "Christophe Raffalli (Laboratoire de Math�matiques, Univ. Savoie)";;
 
let presentation_8 =
  "Formalisation en Coq d'un cours de g�om�trie pour le lyc�e";;
let auteurs_8 = "Fr�d�rique Guilhot (INRIA Sophia-Antipolis)";;
 
let presentation_9 =
  "�valuation de l'extensibilit� de PhoX: B/PhoX \
   un assistant de preuves pour B";;
let auteurs_9 =
  "J�r�me Rocheteau, Samuel Colin, Georges Mariano, Vincent Poirriez";;
 
let presentation_10 =
  "Algorithmes et complexit�s de la r�duction statique minimale";;
let auteurs_10 = "Ludovic Henrio, Bernard Paul Serpette, Szabolcs Szentes";;
 
let presentation_11 =
  "Programmation param�tr�e en ML et automates d'architecture";;
let auteurs_11 = "Philippe Narbel (LaBRI, Bordeaux I)";;

let titre_table_ronde =
  "La place et la pratique du test dans la programmation fonctionnelle";;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)
