(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2008 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

(* The year when we are compiling this program! *)
let year_compile = 2010;;
(* The year when the conference will hold. *)
let year_conf = 2011;;
(* The month when the conference will hold. *)
let month_conf = "janvier";;

let nieme_conf = "vingt deuxi�me";;

(* Dead-lines.
   All these dead-lines are supposed to be in year \$(year_compile)
   (i.e. could be the year before \$(year_conf)). *)
let date_limite_soumission = "17 octobre";;
let date_notification = "19 novembre";;
let date_remise_article = "10 d�cembre";;

(* This deadline implicitely takes place in year \$(year_conf). *)
let date_limite_inscription = "14 janvier";;

(* Dates of the meeting.
   All those dates implicitely takes place in year \$(year_conf). *)
let date_conf = "29 janvier au 1 f�vrier";;
let premier_jour_conf = "31 janvier";;
let deuxieme_jour_conf = "1 f�vrier";;
let date_arrivee_conf = "29 janvier";;

(* Dates des cours.
   All those dates implicitely takes place in year \$(year_conf). *)
let date_arrivee_cours = "29 janvier";;
let jour_cours_1 = "29 janvier";;
let heure_cours_1 = "(Non communiqu�)";;
let jour_cours_2 = "30 janvier";;
let heure_cours_2 = "(Non communiqu�)";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "Sylvain Conchon";;
let president_civilite = "Pr�sident";;
let mail_president_comite = "Sylvain.Conchon@lri.fr";;
let institution_president_comite = "Universit� Paris 11";;

let mail_soumission = "Sylvain.Conchon@lri.fr";;

let phrase_actes =
  "\
   Les articles admis aux JFLA seront l'objet d'une s�lection \
   pour publication journal d'une version longue.\
 \n\
   Les actes des jfla seront remis aux participants durant la conf�rence.\
  "
;;

(* Le vice pr�sident du comit� de programme. *)
let vice_president_comite = "Assia Mahboubi";;
let vice_president_civilite = "Pr�sidente";;
let mail_vice_president_comite = "Assia.Mahboubi@inria.fr";;
let institution_vice_president_comite = "INRIA Saclay -- �le-de-France";;

(* Les membres du comit� de programme.
   Le membre num�ro 0 est le pr�sident.
   Le membre num�ro 1 est le vice-pr�sident.
   Les autres membres commencent donc par le num�ro 2. *)

let membre_comite_2 = "Sylvie Boldo";;
let mail_membre_comite_2 = "sylvie.boldo@inria.fr";;
let institution_membre_comite_2 = "INRIA Saclay -- �le-de-France";;

let membre_comite_3 = "Roberto Di Cosmo";;
let mail_membre_comite_3 = "roberto@dicosmo.org";;
let institution_membre_comite_3 = "Universit� Paris 7";;

let membre_comite_4 = "James Leifer";;
let mail_membre_comite_4 = "James.Leifer@inria.fr";;
let institution_membre_comite_4 = "INRIA Paris -- Rocquencourt";;

let membre_comite_5 = "Alexandre Miquel";;
let mail_membre_comite_5 = "Alexandre.Miquel@pps.jussieu.fr";;
let institution_membre_comite_5 = "ENS Lyon";;

let membre_comite_6 = "David Pichardie";;
let mail_membre_comite_6 = "David.Pichardie@irisa.fr";;
let institution_membre_comite_6 = "INRIA Rennes -- Bretagne Atlantique";;

let membre_comite_7 = "Damien Pous";;
let mail_membre_comite_7 = "Damien.Pous@inria.fr";;
let institution_membre_comite_7 = "CNRS";;

let membre_comite_8 = "Julien Signoles";;
let mail_membre_comite_8 = "Julien.Signoles@cea.fr";;
let institution_membre_comite_8 = "CEA Saclay";;

let membre_comite_9 = "Laurent Th�ry";;
let mail_membre_comite_9 = "Laurent.Thery@sophia.inria.fr";;
let institution_membre_comite_9 = "INRIA Sophia Antipolis -- M�diterran�e";;

(*
let membre_comite_10 = "";;
let mail_membre_comite_10 = "";;
let institution_membre_comite_10 = "";;
*)

(* Les conf�renciers invit�s.
   Le premier est un exemple qui donne le format. *)
let orateur_invite_1 = "Gilles Dowek";;
let mail_orateur_invite_1 = "gilles.dowek@polytechnique.edu";;
let institution_orateur_invite_1 = "�cole Polytechnique";;
let titre_conf_invite_1 = "(� venir)";;

let orateur_invite_2 = "Fran�ois Pottier";;
let mail_orateur_invite_2 = "Francois.Pottier@inria.fr";;
let institution_orateur_invite_2 = "INRIA Paris -- Rocquencourt";;
let titre_conf_invite_2 = "(� venir)";;

(* Les cours et leurs auteurs.
   Le premier est un exemple qui donne le format. *)
let auteur_cours_1 = "Yves Bertot";;
let mail_auteur_cours_1 = "Yves.Bertot@sophia.inria.fr";;
let institution_auteur_cours_1 = "INRIA Sophia Antipolis -- M�diterran�e";;
let titre_cours_1 = "(� venir)"
;;

let auteur_cours_2 = "Luc Maranget";;
let mail_auteur_cours_2 = "Luc.Maranget@inria.fr";;
let institution_auteur_cours_2 = "INRIA Paris -- Rocquencourt";;
let titre_cours_2 = "(� venir)";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
let ou_conf = "� la montagne";;
let lieu_conf = "La Bresse";;
let grande_ville_proche_conf = "Mulhouse";;
let gare_conf = "Remiremont";;
let aeroport_conf = "A�roport B�le/Mulhouse St Louis";;
let distance_aeroport_conf = "� 1h30";;
let autre_aeroport_conf = "A�roport Strasbourg - Entzheim";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "de l'Est";;
let horaires_train_aller_paris = "(Non communiqu�)";;
let horaires_train_retour_paris = "(Non communiqu�)";;
let aeroport_paris = "(Non communiqu�)";;
let distance_de_bordeaux = "930 km";;
let distance_de_lyon = "380 km";;
let distance_de_marseille = "690 km";;
let distance_de_nantes = "790 km";;
let distance_de_paris = "450 km";;
let distance_de_poitiers = "690 km";;
let approche_conf = "(Non Communiqu�)";;

(* Navette et h�tel.
   Respectez bien le format de du nom de l'h�tel. *)
let gare_navette = "(Non Communiqu�)";;
let heure_navette_arrivee = "(Non Communiqu�)";;
let heure_navette_depart = "(Non Communiqu�)";;
let hotel_conf =
  "\
   H�tel Les Vall�es\
   \n  <BR>31 rue Paul Claudel - 88250 La Bresse\
   \n  <BR>T�l. : +33 (0)3 29 25 41 39\
   \n  <BR>Fax : +33 (0)3 29 25 64 38\
   \n  <BR>Email : resa-vallees@labellemontagne.com\
  "
;;

let web_hotel_conf =
  "http://www.lesvallees-labresse.com/"
;;

(* Le contact administratif pour le pr�sident et les conf�renciers. *)
let contact_administratif = "Ga�lle DORKELD";;
let mail_contact_administratif = "Gaelle.DORKELD@inria.fr";;
let adresse_contact_administratif =
  "\
   INRIA Service IST\
\n Domaine de Voluceau - BP 105\
\n 78153 Le Chesnay cedex - France\
  "
;;
let telephone_contact_administratif =
  "Tel : + 33 (0)1 39 63 56 00 - Fax : + 33 (0)1 39 63 56 38"
;;

(* Les donn�es concernant les inscriptions. *)

let mail_reception_inscriptions = "";;

let page_inscriptions = "";;
(*  "http://registration.net-resa.com/cgi-bin/WebObjects/gnetresa.woa/wa/newParticipant?idevt=329&profil=542"
;;*)

(* Une image pour symboliser le lieu de la conf�rence. On la prend en g�n�ral
   sur le site Ou�be du syndicat d'initiative de la ville.

   Cette photo doit �tre mise sous CVS dans les sources du site des jfla,
   sous le nom:
   site_jfla/generation/Data/imgs/pub_site_conf/\$(year_conf)/pub_site_conf.jpg

   Ce doit �tre une image au format jpg de taille raisonnable (inutile de
   charger le r�seau!).

*)
let pub_site_conf =
  "<IMG SRC=\"pub_site_conf.jpg\" ALT=\"Site de la manifestation\">"
;;

(* Une image du site de la conf�rence. En g�n�ral une photo de l'h�tel.

   Cette photo doit �tre mise sous CVS dans les sources du site des jfla,
   sous le nom:
   site_jfla/generation/Data/imgs/site_conf/\$(year_conf)/site_conf.jpg

   Ce doit �tre une image au format jpg de taille raisonnable (inutile de
   charger le r�seau!).

*)
let site_conf =
  "<IMG SRC=\"site_conf.jpg\" ALT=\"Lieu de la conf�rence\">"
;;

let montant_chambre_simple = "(Non communiqu�)";;
let montant_chambre_double = "(Non communiqu�)";;
let montant_devise = "euros";;

let social_event_conf = "une balade";;
let date_social_event_conf = "dimanche apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence.

   Les premiers titre et auteur sont un exemple qui donne le format.

 *)

let presentation_1 =
  ""
;;
let auteurs_1 =
  ""
;;

let presentation_2 =
  ""
;;
let auteurs_2 =
  ""
;;

let presentation_3 =
  ""
;;
let auteurs_3 =
  ""
;;

let presentation_4 =
  ""
;;
let auteurs_4 =
  ""
;;

let presentation_5 =
  ""
;;
let auteurs_5 =
  ""
;;

let presentation_6 =
  ""
;;
let auteurs_6 =
  ""
;;

let presentation_7 =
  ""
;;
let auteurs_7 =
  ""
;;

let presentation_8 =
  ""
;;
let auteurs_8 =
  ""
;;

let presentation_9 =
  ""
;;
let auteurs_9 =
  ""
;;

let presentation_10 =
  ""
;;
let auteurs_10 =
  ""
;;

let presentation_11 =
  ""
;;
let auteurs_11 =
  ""
;;

(* La table ronde s'il y a lieu. *)
let titre_table_ronde =
  ""
;;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)
