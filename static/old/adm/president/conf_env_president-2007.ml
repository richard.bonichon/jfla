(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2007 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

(* The year when we are compiling this program! *)
let this_year = 2005;;
(* The year when the conference will hold. *)
let year_conf = 2006;;

let nieme_conf = "dix-septi�me";;

(* Dead-lines. *)
let date_limite_soumission = "";;
let date_notification = "";;
let date_remise_article = "";;
let date_limite_inscription = "";;

(* Dates of the meeting. *)
let date_conf = "";;
let premier_jour_conf = "";;
let deuxieme_jour_conf = "";;
let date_arrivee_conf = "";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "";;
let mail_president_comite = "";;
let institution_president_comite =
 "LAMI, Universit� �vry-Val d'Essonne, �vry";;
let mail_soumission = "jfla2005@lami.univ-evry.fr";;

(* Les membres du comit� de programme.
   Le membre num�ro 1 est le vice-pr�sident. *)
let membre_comite1 = "Catherine Dubois";;
let mail_membre_comite1 = "dubois@iie.cnam.fr";;
let institution_membre_comite1 = "CEDRIC, Institut d'Informatique d'Entreprise/CNAM";;

let membre_comite2 = "";;
let mail_membre_comite2 = "";;
let institution_membre_comite2 = "";;

let membre_comite3 = "Jacques Garrigue";;
let mail_membre_comite3 = "garrigue@kurims.kyoto-u.ac.jp";;
let institution_membre_comite3 = "Universit� de Kyoto";;

let membre_comite4 = "";;
let mail_membre_comite4 = "";;
let institution_membre_comite4 = "";;

let membre_comite5 = "Pierre-Etienne Moreau";;
let mail_membre_comite5 = "Pierre-Etienne.Moreau@loria.fr";;
let institution_membre_comite5 = "projet PROTHEO, LORIA";;

let membre_comite6 = "";;
let mail_membre_comite6 = "";;
let institution_membre_comite6 = "";;

let membre_comite7 = "Manuel Serrano";;
let mail_membre_comite7 = "Manuel.Serrano@sophia.inria.fr";;
let institution_membre_comite7 = "projet MIMOSA, INRIA Sophia-Antipolis";;

let membre_comite8 = "";;
let mail_membre_comite8 = "";;
let institution_membre_comite8 = "";;

let membre_comite9 = "";;
let mail_membre_comite9 = "";;
let institution_membre_comite9 = "";;

(* Les conf�renciers invit�s. *)
let orateur_invite_1 = "";;
let institution_orateur_invite_1 = "";;
let titre_conf_invite_1 = "(Non communiqu�)";;

let orateur_invite_2 = "";;
let institution_orateur_invite_2 = "";;
let titre_conf_invite_2 = "(Non communiqu�)";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
let ou_conf = "� la mer";;
let lieu_conf = "??? (Pr�s d'�tretat)";;
let grande_ville_proche_conf = "Le Havre";;
let gare_conf = "(Non communiqu�)";;
let aeroport_conf = "(Non communiqu�)";;
let distance_aeroport_conf = "(Non communiqu�)";;
let autre_aeroport_conf = "";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "(Non communiqu�)";;
let horaires_train_aller_paris = "(Non communiqu�)";;
let horaires_train_retour_paris = "(Non communiqu�)";;
let aeroport_paris = "(Non communiqu�)";;
let distance_de_bordeaux = "(Non communiqu�)";;
let distance_de_lyon = "(Non communiqu�)";;
let distance_de_marseille = "(Non communiqu�)";;
let distance_de_nantes = "(Non communiqu�)";;
let distance_de_paris = "250 km";;
let distance_de_poitiers = "(Non communiqu�)";;
let approche_conf = "(Non Communiqu�)";;

(* Navette et h�tel. *)
let gare_navette = "(Non communiqu�)";;
let heure_navette_arrivee = "17h30";;
let heure_navette_depart = "15h30";;
let hotel_conf = "VVF Villages � Les G�raniums �";;
let web_hotel_conf =
  "http://www.obernai.fr/site/decouvrir/index.php4?la=fr&\
   sais=automne&la=fr&partie=2";;
let adresse_hotel_conf =
  printf__sprintf "\
<BLOCKQUOTE>
%s<BR>
2 rue de Berlin<BR>
BP 43 67212 OBERNAI Cedex<BR>
Tel: 03 88 49 45 45<BR>
Fax: 03 88 49 90 32<BR>
%s
</BLOCKQUOTE>
" hotel_conf web_hotel_conf;;

(* Une image pour symboliser le lieu de la conf�rence. On la prend en g�n�ral
   sur le site Ou�be du syndicat d'initiative. *)
let pub_site_conf =
  "<IMG SRC=\"pub_site_conf.jpg\" ALT=\"Site de la manifestation\">";;

let montant_chambre_simple = "230";;
let montant_chambre_double = "190";;
let montant_devise = "euros";;

let social_event_conf = "une balade � pied ou en v�lo";;
let date_social_event_conf = "lundi " ^ premier_jour_conf ^ " apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence. *)
let presentation_1 =
  "Ocaml-templates, g�n�ration de code � partir des types";;
let auteurs_1 = "Fran�ois Maurel (PPS, Paris 7)";;

let presentation_2 =
  "Une proc�dure de d�cision r�flexive pour un fragment \
   de l'arithm�tique de Presburger";;
let auteurs_2 =
  "Pierre Cr�gut (France-T�l�com R & D - DTL/TAL)";;
 
let presentation_3 =
  "";;
let auteurs_3 = "";;
 
let presentation_4 =
  "";;
let auteurs_4 =
  "";;

let presentation_5 =
  "";;
let auteurs_5 =
  "";;
 
let presentation_6 =
  "";;
let auteurs_6 =
  "";;
 
let presentation_7 =
  "";;
let auteurs_7 =
  "";;
 
let presentation_8 =
  "";;
let auteurs_8 =
  "";;
 
let presentation_9 =
  "";;
let auteurs_9 =
  "";;
 
let presentation_10 =
  "";;
let auteurs_10 =
  "";;
 
let presentation_11 =
  "";;
let auteurs_11 =
  "";;

let titre_table_ronde =
  "La place et la pratique du test dans la programmation fonctionnelle";;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)
