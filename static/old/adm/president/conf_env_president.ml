(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2011 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* \$Id: conf_env_president.ml,v 1.16 2011-09-02 13:44:01 weis Exp $ *)

(* This file is under cvs control and resides in the directory

   ~jfla/src/site_jfla/adm/president/


This file is a legal Caml program: as such you MUST respect the format given;
in fact, you have just to write data inside character strings (between the
opening symbol " and the closing symbol ").

Furthermore, the various strange characters, the backslashes ('\'), the new
lines and the explicit '\n' written in the strings are mandatory and intentional:
they must be preserved to obtain a working file.

Rule of thumb: only change purely alphanumeric characters inside the
character strings, left the rest untouched.

Warning: accented letters should be ISO-8859-1 codes, as specified for Caml
strings. Be careful not to use DOS or MacOS specific encodings.

Examples:
- never modify characters in Caml remarks (the explaining text enclosed between (* and *))
- in the string
  "\
   INRIA Service IST\
\n Domaine de Voluceau - BP 105\
\n 78153 Le Chesnay cedex - France\
  "
  the \n characters starting the lines and the \ at each end of line are not
  random garbbage: they are mandatory and should not be removed.

*)

(* This file contains ALL the data necessary to generate:

- the Web site of the conference for the given year,
- the call for papers,
- the form to subscribe to the Conference JFLA.

*)

(* The president of the conference must fill this file when information becomes
   available.  *)

(* The year when we are compiling this program! *)
let year_compile = 2011;;
(* The year when the conference will hold. *)
let year_conf = 2012;;
(* The month when the conference will hold. *)
let month_conf = "f�vrier";;

let nieme_conf = "vingt troisi�me";;

(* Dead-lines.
   All these dead-lines dates are supposed to hold during the year
   \$(year_compile) (i.e. could be the year before \$(year_conf)). *)
let date_limite_soumission = "16 octobre";;
let date_notification = "18 novembre";;
let date_remise_article = "9 d�cembre";;

(* This deadline implicitely takes place in year \$(year_conf). *)
let date_limite_inscription = "20 janvier";;

(* Dates of the meeting.
   All those dates implicitely takes place in year \$(year_conf). *)
let date_conf = "4 f�vrier au 7 f�vrier";;
let premier_jour_conf = "6 f�vrier";;
let deuxieme_jour_conf = "7 f�vrier";;
let date_arrivee_conf = "4 f�vrier";;

(* Dates for JFLA lectures.
   All those dates implicitely takes place in year \$(year_conf). *)
let date_arrivee_cours = "4 f�vrier";;
let jour_cours_1 = "4 f�vrier";;
let heure_cours_1 = "(Non communiqu�)";;
let jour_cours_2 = "5 f�vrier";;
let heure_cours_2 = "(Non communiqu�)";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "Assia Mahboubi";;
let president_civilite = "Pr�sidente";;
let mail_president_comite = "Assia.Mahboubi@inria.fr";;
let institution_president_comite = "INRIA Saclay -- �le-de-France";;

let mail_soumission = "Assia.Mahboubi@inria.fr";;

(* Respectez bien le format de la cha�ne de caract�res donnant la phrase
   concernant les actes de la conf�rence:
   Les divers backslash ('\'), retour chariots et \n sont n�cessaires et intentionnels. *)
let phrase_actes =
  "\
   Les articles admis aux JFLA seront l'objet d'une s�lection \
   pour publication journal d'une version longue.\
 \n\
   Les actes des jfla seront remis aux participants durant la conf�rence.\
  "
;;

(* Le vice pr�sident du comit� de programme. *)
let vice_president_comite = "Damien Pous";;
let vice_president_civilite = "Vice pr�sident";;
let mail_vice_president_comite = "Damien.Pous@inria.fr";;
let institution_vice_president_comite = "CNRS";;

(* Les membres du comit� de programme.
   Le membre num�ro 0 est le pr�sident.
   Le membre num�ro 1 est le vice-pr�sident.
   Les autres membres commencent donc par le num�ro 2. *)

let membre_comite_2 = "David Baelde";;
let mail_membre_comite_2 = "David.Baelde@ens-lyon.org";;
let institution_membre_comite_2 = "Universti� Paris Sud 11";;

let membre_comite_3 = "Fr�deric Besson";;
let mail_membre_comite_3 = "Frederic.Besson@inria.fr";;
let institution_membre_comite_3 = "INRIA Rennes -- Bretagne Atlantique";;

let membre_comite_4 = "Sandrine Blazy"
let mail_membre_comite_4 = "Sandrine.Blazy@irisa.fr";;
let institution_membre_comite_4 = "Universit� Rennes 1";;

let membre_comite_5 = "Louis Mandel";;
let mail_membre_comite_5 = "Louis.Mandel@lri.fr";;
let institution_membre_comite_5 = "Universit� Paris Sud 11";;

let membre_comite_6 = "Guillaume Melquiond";;
let mail_membre_comite_6 = "Guillaume.Melquiond@inria.fr";;
let institution_membre_comite_6 = "INRIA Saclay -- �le-de-France";;

let membre_comite_7 = "Julien Narboux";;
let mail_membre_comite_7 = "Julien.Narboux@lsiit-cnrs.unistra.fr";;
let institution_membre_comite_7 = "Universit� de Strasbourg";;

let membre_comite_8 = "Yann R�gis-Gianas";;
let mail_membre_comite_8 = "Yann.Regis-Gianas@pps.jussieu.fr";;
let institution_membre_comite_8 = "Universit� Paris 7";;

let membre_comite_9 = "Christine Tasson";;
let mail_membre_comite_9 = "tasson@pps.jussieu.fr";;
let institution_membre_comite_9 = "Universit� Paris 7";;

(* S'il y a lieu
let membre_comite_10 = "(Non communiqu�)";;
let mail_membre_comite_10 = "(Non communiqu�)";;
let institution_membre_comite_10 = "(Non communiqu�)";;
*)

(* Les conf�renciers invit�s. *)
let orateur_invite_1 = "Dimitrios Vytiniotis";;
let mail_orateur_invite_1 = "dimitris@microsoft.com";;
let institution_orateur_invite_1 = "Microsoft Research";;
let titre_conf_invite_1 = "(Non communiqu�)";;

let orateur_invite_2 = "(Non communiqu�)";;
let mail_orateur_invite_2 = "(Non communiqu�)";;
let institution_orateur_invite_2 = "(Non communiqu�)";;
let titre_conf_invite_2 = "(Non communiqu�)";;

(* Les cours et leurs auteurs.
   Le premier est un exemple qui donne le format. *)
let auteur_cours_1 = "Jean-Christophe Filli�tre";;
let mail_auteur_cours_1 = "Jean-Christophe.Filliatre@lri.fr";;
let institution_auteur_cours_1 = "CNRS";;
let titre_cours_1 = "(Non communiqu�)"
;;

let auteur_cours_2 = "Matthieu Sozeau";;
let mail_auteur_cours_2 = "Matthieu.Sozeau@inria.fr";;
let institution_auteur_cours_2 = "INRIA Paris -- Rocquencourt";;
let titre_cours_2 = "(Non communiqu�)";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
(* ou_conf = "� la mer" ou "� la montagne" ou "� la campagne" *)
let ou_conf = "� la mer";;
(* lieu_conf est la ville ou le village o� a lieu la conf. *)
let lieu_conf = "(Non communiqu�)";;
let grande_ville_proche_conf = "(Non communiqu�)";;
let gare_conf = "(Non communiqu�)";;
let aeroport_conf = "(Non communiqu�)";;
let distance_aeroport_conf = "(Non communiqu�)";;
let autre_aeroport_conf = "(Non communiqu�)";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "(Non communiqu�)";;
let horaires_train_aller_paris = "(Non communiqu�)";;
let horaires_train_retour_paris = "(Non communiqu�)";;
let aeroport_paris = "(Non communiqu�)";;
let distance_de_bordeaux = "(Non communiqu�)";;
let distance_de_lyon = "(Non communiqu�)";;
let distance_de_marseille = "(Non communiqu�)";;
let distance_de_nantes = "(Non communiqu�)";;
let distance_de_paris = "(Non communiqu�)";;
let distance_de_poitiers = "(Non communiqu�)";;
let approche_conf = "(Non Communiqu�)";;

(* Navette et h�tel. *)
let gare_navette = "(Non Communiqu�)";;
let heure_navette_arrivee = "(Non Communiqu�)";;
let heure_navette_depart = "(Non Communiqu�)";;

(* Respectez bien le format de la cha�ne de caract�res donnant le nom de
   l'h�tel:
   Les divers backslashs ('\'), retour chariots et \n sont n�cessaires et
   intentionnels, tout comme les �tranges balises <BR>.
   Ne modifiez que les caract�res d�finissant �videmment les coordonn�es de
   l'h�tel: adresse, num�ros de t�l�phone et fax, email. *)
let hotel_conf =
  "\
   H�tel Les Plages\
   \n  <BR>31 rue Paul ...\
   \n  <BR>T�l. : +33 (0)3 .. .. .. ..\
   \n  <BR>Fax : +33 (0)3 .. .. .. ..\
   \n  <BR>Email : ...@... .com\
  "
;;

let web_hotel_conf =
  "http://www. ... .com/"
;;

(* Le contact administratif pour le pr�sident et les conf�renciers.

   Respectez bien le format des cha�nes de caract�res:
   les divers backslashs ('\'), retour chariots et \n sont n�cessaires et
   intentionnels, tout comme les �tranges balises <BR>.
   Ne modifiez que les caract�res d�finissant �videmment les coordonn�es du
   contact administratif: nom, adresse, num�ros de t�l�phone.
*)
let contact_administratif = "Chantal Girodon";;
let mail_contact_administratif = "Chantal.Girodon@inria.fr";;
let adresse_contact_administratif =
  "\
   INRIA Service IST\
\n Domaine de Voluceau - BP 105\
\n 78153 Le Chesnay cedex - France\
  "
;;
let telephone_contact_administratif =
  "Tel : +33 (0)1 39 63 50 53 - Fax : +33 (0)1 39 63 56 38"
;;

(* Les donn�es concernant les inscriptions. *)

let mail_reception_inscriptions = "";;

let page_inscriptions = "";;

(* Une image pour symboliser le site de la conf�rence. On la prend en g�n�ral
   sur le site Ou�be du syndicat d'initiative de la ville.

   Cette photo doit �tre mise sous CVS dans les sources du site des jfla,
   sous le nom:

   ~jfla/src/site_jfla/adm/president/photo_site_conf.jpg

   Ce doit �tre une image au format jpg de taille raisonnable (inutile de
   charger le r�seau!).
*)
let photo_site_conf =
  "<IMG SRC=\"photo_site_conf.jpg\" ALT=\"Photo du site de la manifestation\">"
;;

(* Une image du lieu m�me de la conf�rence. En g�n�ral une photo de l'h�tel.

   Cette photo doit �tre mise sous CVS dans les sources du site des jfla,
   sous le nom:

   ~jfla/src/site_jfla/adm/president/photo_hotel_conf.jpg

   Ce doit �tre une image au format jpg de taille raisonnable (inutile de
   charger le r�seau!).
*)
let photo_hotel_conf =
  "<IMG SRC=\"photo_hotel_conf.jpg\" ALT=\"Photo du lieu de la conf�rence\">"
;;

let montant_chambre_simple = "(Non communiqu�)";;
let montant_chambre_double = "(Non communiqu�)";;
let montant_devise = "euros";;

let social_event_conf = "une balade";;
let date_social_event_conf = "dimanche apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence.

   Les premiers titre et auteur sont un exemple qui donne le format.

 *)

let presentation_1 =
  ""
;;
let auteurs_1 =
  ""
;;

let presentation_2 =
  ""
;;
let auteurs_2 =
  ""
;;

let presentation_3 =
  ""
;;
let auteurs_3 =
  ""
;;

let presentation_4 =
  ""
;;
let auteurs_4 =
  ""
;;

let presentation_5 =
  ""
;;
let auteurs_5 =
  ""
;;

let presentation_6 =
  ""
;;
let auteurs_6 =
  ""
;;

let presentation_7 =
  ""
;;
let auteurs_7 =
  ""
;;

let presentation_8 =
  ""
;;
let auteurs_8 =
  ""
;;

let presentation_9 =
  ""
;;
let auteurs_9 =
  ""
;;

let presentation_10 =
  ""
;;
let auteurs_10 =
  ""
;;

let presentation_11 =
  ""
;;
let auteurs_11 =
  ""
;;

(* La table ronde s'il y a lieu. *)
let titre_table_ronde =
  ""
;;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)

(* Notes:

** This file is a legal htmlc environment file that is used during the
   generation of the Web pages of the conference for this year.
   Remember that the Web site is dynamic in the sense that there are 3
   successive stages of the site (and 6 different views):

   (1) Before the article selection and review:

   - ``initial'': the initial one (not everything is decided yet).
   - ``premier_appel'': the first call to communication has been generated and
     sent; the first call to communication is available on-line now on this site.
   - ``deuxieme_appel'': same as the preceding one for the second call for participation.

   (2) Before the conference:
   - ``inscriptions'': now everything has been set up; articles has been reviewd and
     selected, the final program of the conference is also set and is made
     available on-line on this site (mandatory, since this is often necessary
     to subscribe to the conference); the form to subscribe is available
     on-line on this site (if desirable, the president can do otherwise if
     more appropriate).
   - ``inscriptions_closes'': a slight evolution of the preceeding one, since the
     form is no more available.

   (3) After the conference:
   - ``final'': the final site (the conference has been held and this site gives access
   to the on-line proceedings).


** As a side effect, this file is also used to compile various text files including:
   - the LaTeX style for this year,
   - the various call for communication (first and second one).

** This file is also a legal Caml program populates the Caml module that
   generate the Web form to subscribe to the conference.

   Each variable listed in this environment, is replaced by the associated value
   string, during the expansion phase from .data to .html files or from .data
   to .txt or .sty files.
*)
