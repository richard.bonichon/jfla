(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2008 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

(* The year when we are compiling this program! *)
let this_year = 2009;;
(* The year when the conference will hold. *)
let year_conf = 2010;;
(* The month when the conference will hold. *)
let month_conf = "janvier";;

let nieme_conf = "vingt et uni�me";;

(* Dead-lines.
   All these dead-lines are supposed to be in year this_year
   (i.e. could be the year before year_conf). *)
let date_limite_soumission = "15 octobre";;
let date_notification = "15 novembre";;
let date_remise_article = "10 d�cembre";;

(* This deadline implicitely takes place in year year_conf. *)
let date_limite_inscription = "15 janvier";;

(* Dates of the meeting.
   All those dates implicitely takes place in year year_conf. *)
let date_conf = "30 janvier au 2 f�vrier";;
let premier_jour_conf = "1 f�vrier";;
let deuxieme_jour_conf = "2 f�vrier";;
let date_arrivee_conf = "30 janvier";;

(* Dates des cours.
   All those dates implicitely takes place in year year_conf. *)
let date_arrivee_cours = "30 janvier";;
let jour_cours_1 = "30 janvier";;
let heure_cours_1 = "(Non communiqu�)";;
let jour_cours_2 = "31 janvier";;
let heure_cours_2 = "(Non communiqu�)";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "Micaela Mayero";;
let president_civilite = "Pr�sidente";;
let mail_president_comite = "Micaela.Mayero@lipn.univ-paris13.fr";;
let institution_president_comite = "Universit� Paris 13"
;;
let mail_soumission = "Micaela.Mayero@lipn.univ-paris13.fr";;

(* Le vice pr�sident du comit� de programme. *)
let vice_president_comite = "Sylvain Conchon";;
let vice_president_civilite = "Pr�sident";;
let mail_vice_president_comite = "Sylvain.Conchon@lri.fr";;
let institution_vice_president_comite = "Universit� Paris 11";;

(* Les membres du comit� de programme.
   Le membre num�ro 1 est le vice-pr�sident. *)

let membre_comite_2 = "Assia Mahboubi";;
let mail_membre_comite_2 = "Assia.Mahboubi@inria.fr";;
let institution_membre_comite_2 = "INRIA Saclay";;

let membre_comite_3 = "Alan Schmitt";;
let mail_membre_comite_3 = "alan.schmitt@inrialpes.fr";;
let institution_membre_comite_3 = "INRIA Grenoble - Rh�ne-Alpes";;

let membre_comite_4 = "Pierre Courtieu";;
let mail_membre_comite_4 = "Pierre.Courtieu@cnam.fr";;
let institution_membre_comite_4 = "CNAM Paris";;

let membre_comite_5 = "Damien Pous";;
let mail_membre_comite_5 = "Damien.Pous@inria.fr";;
let institution_membre_comite_5 = "INRIA Grenoble - Rh�ne-Alpes";;

let membre_comite_6 = "Bruno Barras";;
let mail_membre_comite_6 = "barras@lix.polytechnique.fr";;
let institution_membre_comite_6 = "�cole Polytechnique";;

let membre_comite_7 = "Bernard Serpette";;
let mail_membre_comite_7 = "Bernard.Serpette@inria.fr";;
let institution_membre_comite_7 = "INRIA Sophia Antipolis - M�diterran�e";;

let membre_comite_8 = "Manuel Serrano";;
let mail_membre_comite_8 = "Manuel.Serrano@sophia.inria.fr";;
let institution_membre_comite_8 = "INRIA Sophia Antipolis - M�diterran�e";;

let membre_comite_9 = "Damien Doligez";;
let mail_membre_comite_9 = "Damien.Doligez@inria.fr";;
let institution_membre_comite_9 = "INRIA Paris - Rocquencourt";;

(*
let membre_comite_10 = "";;
let mail_membre_comite_10 = "";;
let institution_membre_comite_10 = "";;
*)

(* Les conf�renciers invit�s. *)
let orateur_invite_1 = "Leslie Lamport";;
let mail_orateur_invite_1 = "lamport@microsoft.com";;
let institution_orateur_invite_1 = "Microsoft";;
let titre_conf_invite_1 = "(� venir)";;

let orateur_invite_2 = "Christian Queinnec";;
let mail_orateur_invite_2 = "Christian.Queinnec@lip6.fr";;
let institution_orateur_invite_2 = "Universite Paris 6";;
let titre_conf_invite_2 = "(� venir)";;

(* Les cours et leurs auteurs. *)
let auteur_cours_1 = "Pierre Letouzey";;
let mail_auteur_cours_1 = "Pierre.Letouzey@pps.jussieu.fr";;
let institution_auteur_cours_1 = "Universit� paris 7";;
let titre_cours_1 = "(� venir)"
;;

let auteur_cours_2 = "Louis Mandel";;
let mail_auteur_cours_2 = "Louis.Mandel@lri.fr";;
let institution_auteur_cours_2 = "Universite Paris 11";;
let titre_cours_2 = "(� venir)";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
let ou_conf = "� la mer";;
let lieu_conf = "Vieux-Port La Ciotat";;
let grande_ville_proche_conf = "Marseille";;
let gare_conf = "La Ciotat";;
let aeroport_conf = "A�roport Marseille Provence";;
let distance_aeroport_conf = "(Non communiqu�)";;
let autre_aeroport_conf = "A�roport Toulon Hy�res";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "de Lyon";;
let horaires_train_aller_paris = "7h15 - 11h36 ou 8h16 - 12h01";;
let horaires_train_retour_paris = "17h02 - 21h31 ou 17h23 - 21h31";;
let aeroport_paris = "(Non communiqu�)";;
let distance_de_bordeaux = "700 km";;
let distance_de_lyon = "350 km";;
let distance_de_marseille = "30 km";;
let distance_de_nantes = "1000 km";;
let distance_de_paris = "800 km";;
let distance_de_poitiers = "800 km";;
let approche_conf = "(Non Communiqu�)";;

(* Navette et h�tel. *)
let gare_navette = "(Non Communiqu�)";;
let heure_navette_arrivee = "(Non Communiqu�)";;
let heure_navette_depart = "(Non Communiqu�)";;
let hotel_conf = "H�tel Best Western Premier Vieux-Port
<BR>Vieux Port - 13600 La Ciotat  
<BR>T�l. : +33 (0)4 42 04 00 00
<BR>Fax : +33 (0)4 42 04 00 02 
<BR>Email : vieux-port@hotel-ciotat.com"
;;

let web_hotel_conf =
  "http://www.bestwestern-laciotat.com/"
;;
let adresse_hotel_conf =
  Printf.sprintf "\
<BLOCKQUOTE>
%s<BR>
%s
</BLOCKQUOTE>
" hotel_conf web_hotel_conf
;;

(* Le contact administratif pour le pr�sident et les conf�renciers. *)
let contact_administratif = "";;
let mail_contact_administratif = "colloques@inrialpes.fr";;
let adresse_contact_administratif = "\
  INRIA Grenoble Rh�ne-Alpes\n\
  Bureau des Cours-Colloques\n\
  655 avenue de l'Europe - Montbonnot\n\
  38334 Saint Ismier Cedex - France\n"
;;
let telephone_contact_administratif =
  "Tel : + 33 (0)4 76 61 52 23 - Fax : + 33 (0)4 76 61 52 06"
;;
let mail_reception_inscriptions = "colloques@inrialpes.fr";;

let page_inscriptions =
  "http://registration.net-resa.com/cgi-bin/WebObjects/gnetresa.woa/wa/newParticipant?idevt=329&profil=542"
;;

(* Une image pour symboliser le lieu de la conf�rence. On la prend en g�n�ral
   sur le site Ou�be du syndicat d'initiative. *)
let pub_site_conf =
  "<IMG SRC=\"pub_site_conf.jpg\" ALT=\"Site de la manifestation\">"
;;
(* Une image du site de la conf�rence. En g�n�ral une photo de l'h�tel. *)
let site_conf =
  "<IMG SRC=\"site_conf.jpg\" ALT=\"Lieu de la conf�rence\">"
;;

let montant_chambre_simple = "550 (� confirmer)";;
let montant_chambre_double = "(Non communiqu�)";;
let montant_devise = "euros";;

let social_event_conf = "une balade";;
let date_social_event_conf = "dimanche " ^ premier_jour_conf ^ " apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence. *)

let presentation_1 =
  "";;
let auteurs_1 = "";;

let presentation_2 =
  "";;
let auteurs_2 =
  "";;
 
let presentation_3 =
  "";;
let auteurs_3 = "";;
 
let presentation_4 =
  "";;
let auteurs_4 =
  "";;

let presentation_5 =
  "";;
let auteurs_5 =
  "";;
 
let presentation_6 =
  "";;
let auteurs_6 =
  "";;
 
let presentation_7 =
  "";;
let auteurs_7 =
  "";;
 
let presentation_8 =
  "";;
let auteurs_8 =
  "";;
 
let presentation_9 =
  "";;
let auteurs_9 =
  "";;
 
let presentation_10 =
  "";;
let auteurs_10 =
  "";;
 
let presentation_11 =
  "";;
let auteurs_11 =
  "";;

let titre_table_ronde =
  "";;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)
