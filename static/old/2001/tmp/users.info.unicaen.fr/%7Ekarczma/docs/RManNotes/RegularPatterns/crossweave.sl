/* crossweave.sl
 *
 */
#include "rmannotes.sl"

surface crossweave()
{
  color surface_color, layer_color;
  color surface_opac, layer_opac;
  float fuzz = 0.05;
  float ss, tt;
  float row, col;
  float freq = 4;

  /* background color */

  surface_color = color (0.1, 0.1, 0.3);
  surface_opac = 1;

  /* repeat pattern 'freq' times horizontally & vertically */

  ss = repeat(s, freq);
  tt = repeat(t, freq);
  col = whichtile(s, freq);
  row = whichtile(t, freq);

  if (even(row) && odd(col) || even(col) && odd(row)) {

    /* vertical bar */

    layer_color = 1;
    layer_opac = pulse(0.35, 0.65, fuzz, ss);
    surface_color = blend(surface_color, layer_color, layer_opac);

    /* horizontal bar */

    layer_color = 0.5;
    layer_opac = pulse(0.35, 0.65, fuzz, tt);
    surface_color = blend(surface_color, layer_color, layer_opac);
  }
  else {

    /* horizontal bar */

    layer_color = 0.5;
    layer_opac = pulse(0.35, 0.65, fuzz, tt);
    surface_color = blend(surface_color, layer_color, layer_opac);

    /* vertical bar */

    layer_color = 1;
    layer_opac = pulse(0.35, 0.65, fuzz, ss);
    surface_color = blend(surface_color, layer_color, layer_opac);
  }

  /* output */

  Oi = surface_opac;
  Ci = surface_opac * surface_color;
}
