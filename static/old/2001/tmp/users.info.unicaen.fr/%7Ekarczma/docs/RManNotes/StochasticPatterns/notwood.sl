/* notwood.sl
 *
 * wood.sl without perturbation
 *
 */
#include "rmannotes.sl"

surface notwood(float ringscale = 5;
		color lightwood = color (0.3, 0.12, 0.03),
		darkwood = color (0.05, 0.01, 0.005);
		float Ka = 0.2, Kd = 0.4, Ks = 0.6, roughness = 0.1)
{
  color surface_color, layer_color;
  color layer_opac;
  point Nf, V;
  point PP;
  float r;

  /* init */

  surface_color = lightwood;

  Nf = faceforward(normalize(N), I);
  V = -normalize(I);

  /* transform P to shader space */

  PP = transform("shader", P);

  /* compute distance from x axis */

  r = distance((0,ycomp(PP),zcomp(PP)), (0,0,0));

  /* create concentric cylinders */

  r = repeat(r, ringscale); 

  layer_color = darkwood;
  layer_opac = smoothstep(0,0.8,r) - smoothstep(0.83,1,r);
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* plastic illum w/variation of shininess based on rings */

  surface_color = surface_color * (Ka * ambient() + Kd * diffuse(Nf)) +
    (0.3 * r + 0.7) * Ks * specular(Nf, V, roughness);

  /* output */

  Oi = Os;
  Ci = Os * surface_color;
}
	     
