/* line.sl
 *
 * generate line shape with specified endpoints (p1 & p2)
 *
 */
#include "rmannotes.sl"

surface line()
{
  color surface_color, layer_color;
  color layer_opac;
  float fuzz = 0.025;
  color green = color (0,0.5,0);
  point p1, p2;
  float half_width;
  float d;

  surface_color = Cs;

  layer_color = green;
  p1 = (0.25, 0.15, 0);   /* endpoint #1 */
  p2 = (0.85, 0.7, 0);    /* endpoint #2 */
  half_width = 0.05;      /* 1/2 line width */
  d = ptlined(p1, p2, (s, t, 0));
  layer_opac = 1 - smoothstep(half_width - fuzz, half_width, d);
  surface_color = blend(surface_color, layer_color, layer_opac);

  Ci = surface_color;
}
