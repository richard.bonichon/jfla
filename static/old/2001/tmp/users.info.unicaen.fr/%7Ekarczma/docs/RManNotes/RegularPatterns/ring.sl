/* ring.sl
 *
 * generate ring shape with specified radius, width, and center
 *
 */
#include "rmannotes.sl"

surface ring()
{
  color surface_color, layer_color;
  color layer_opac;
  float fuzz = 0.025;
  color blue = color (0,0,1);
  point center;
  float radius, half_width;
  float d;

  surface_color = Cs;

  layer_color = blue;
  center = (0.5, 0.5, 0);  /* position of ring */
  radius = 0.35;           /* radius of ring */
  half_width = 0.05;       /* 1/2 width of ring */
  d = distance(center, (s, t, 0));
  layer_opac = pulse(radius - half_width, radius + half_width, fuzz, d);
  surface_color = blend(surface_color, layer_color, layer_opac);

  Ci = surface_color;
}
