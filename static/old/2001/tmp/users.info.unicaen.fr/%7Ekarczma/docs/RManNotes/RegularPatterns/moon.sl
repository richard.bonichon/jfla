/* moon.sl
 *
 * crescent moon shape using difference boolean operator
 * 
 */
#include "rmannotes.sl"

surface moon()
{
  color surface_color, layer_color;
  color layer_opac;
  float fuzz = 0.01;
  float circle1, circle2, radius, d;
  point center;

  surface_color = color (0.05, 0.05, 0.15);

  fuzz = 0.01;

  radius = 0.45;             /* moon radius */
  layer_color = color(1, 1, 0.9);  /* moon color */

  /* first circle */

  center = (.5, .5, 0);
  d = distance(center, (s, t, 0));
  circle1 = 1 - smoothstep(radius - fuzz, radius, d);

  /* second circle */

  center = (.65, .5, 0);
  d = distance(center, (s, t, 0));
  circle2 = 1 - smoothstep(radius - fuzz, radius, d);

  /* use difference of two circles to create moon */

  layer_opac = difference(circle1, circle2);
  surface_color = blend(surface_color, layer_color, layer_opac);

  Ci = surface_color;
}
