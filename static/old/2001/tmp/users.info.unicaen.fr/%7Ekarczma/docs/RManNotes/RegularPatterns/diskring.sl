/* diskring.sl
 *
 * masking effect achieved using boolean ops 
 *
 */
#include "rmannotes.sl"

surface diskring()
{
  color surface_color, layer_color;
  color layer_opac;
  float fuzz = 0.01;
  point center;
  float radius, d, half_width;
  float disk, ring;
  float ss, tt;

  surface_color = 0;

  /* disk */

  ss = repeat(s, 2);
  tt = repeat(t, 2);
  center = (0.5, 0.5, 0);
  radius = 0.35;
  d = distance(center, (ss, tt, 0));
  disk = 1 - smoothstep(radius - fuzz, radius, d);

  /* ring */

  ss = repeat(s, 5);
  tt = repeat(t, 5);
  center = (0.5, 0.5, 0);
  radius = 0.35;
  half_width = 0.05;
  d = distance(center, (ss, tt, 0));
  ring = pulse(radius - half_width, radius + half_width, fuzz, d);

  /* bool disk & ring */

  layer_color = color (1, 0.2, 0.2);
  layer_opac = union(difference(disk, ring), difference(ring, disk));
  surface_color = blend(surface_color, layer_color, layer_opac);

  Ci = surface_color;
}
