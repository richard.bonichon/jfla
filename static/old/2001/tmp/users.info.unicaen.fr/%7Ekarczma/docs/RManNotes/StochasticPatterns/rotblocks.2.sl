/* rotblocks.2.sl
 *
 * bombed, rotated squares
 *
 */
#include "rmannotes.sl"

surface rotblocks()
{
  color surface_color, layer_color;
  color layer_opac;
  float ss, tt, tmps, tmpt;
  float col, row;
  float fuzz = 0.02;
  float freq = 4;
  float noi;

  surface_color = 0; /* black background */

  /* base seed to udn on current row and column of tile */

  col = whichtile(s, freq);
  row = whichtile(t, freq);
  noi = noise(col * 10 + 0.5, row * 10 + 0.5);

  /* repeat texture coords, jitter tiles by plus or minus 0.35, 
     and rotate by 45 */

  tmps = repeat(s, freq) + udn(noi * 1183, -0.35, 0.35);
  tmpt = repeat(t, freq) + udn(noi * 999, -0.35, 0.35);

  rotate2d(tmps, tmpt, radians(45), 0.5, 0.5, ss, tt);

  /* generate tile */

  layer_color = color (0.1, 1, 0.1);
  layer_opac = intersection(pulse(0.35, 0.65, fuzz, ss), 
			  pulse(0.35, 0.65, fuzz, tt));
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* output */

  Ci = surface_color;
}
