/* disk.sl
 *
 * generate disk shape with specified radius and center
 *
 */
#include "rmannotes.sl"

surface disk()
{
  color surface_color, layer_color;
  color layer_opac;
  float fuzz = 0.025;
  color blue = color (0,0,1);
  point center;
  float radius;
  float d;

  surface_color = Cs;

  layer_color = blue;
  center = (0.5, 0.5, 0);  /* location of center of disk */
  radius = 0.35;           /* radius of disk */
  d = distance(center, (s, t, 0));
  layer_opac = 1 - smoothstep(radius - fuzz, radius, d);
  surface_color = blend(surface_color, layer_color, layer_opac);

  Ci = surface_color;
}
