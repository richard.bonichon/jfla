<html>
<head>
<title>RManNotes: Regular Patterns: Mod</title>
</head>

<body bgcolor="#f9f9b1" background="notepad.gif" link="801010" vlink="101080" alink="108010"> <img alt="RManNotes" align=center src="header.gif">

<img align=center hspace=10 src="reg_patterns.gif">

<p><img src="black_line.gif"></p>
<p>
<a href="transitions.html">previous</a>
|
<a href="txtr_coords.html">next</a>
|
<a href="index.html">regular patterns: <b>contents</b></a>
|
<a href="../index.html">rmannotes: <b>top</b></a>
</i>
</p>
<p><img src="black_line.gif"></p>

<h1><a name="top">Tiling Using Mod</a></h1>

<p>
The <b><i>mod function</i></b> is probably the most important building block 
for generating periodic patterns. <code>mod</code> produces a sawtooth pattern 
which repeats from 0 to <i>a</i> based on the variable <i>x</i>. 
</p>

<p align=center>
<img src="mod.gif"><br>
<code>mod(<i>x</i>, <i>a</i>)</code>, where <code><i>a</i></code> = 1</center>
</p>

<p>
When <code>mod</code> is used to modify the input to another function, <i>f</i>, the result is that function <i>f</i> gets repeated.
</p>

<p align=center>
<img src="mod.2.gif"><br>
(a) shows a simple function, <code>f(x)</code>. 
(b) shows <code>f(mod(x * 5, 1.0))</code><br>demonstrating the use of <code>mod</code>
to repeat a function.
<br>
<font size=-1>Adapted from 
<a href="../references.html">[Apodaca92]</a>.</font>
</p>

<h2><a name="sstt">Tile Texture Coordinates</h2>

<p>
<code>mod</code> can also be used to repeat tiles over an entire surface. Instead of using texture coordinates (s, t) which 
vary from 0 to 1 over the entire surface to compute a tile pattern, we use 
<b><i>tile texture coordinates</i></b> (ss, tt) which vary 
from 0 to 1 <i>repeatedly</i> over the surface. 
</p>

<p align=center>
<img src="tile_textcoords.gif"><br>
(a) Texture coordinates (s,t) over entire surface;<br>
(b) tile texture coordinates (ss, tt) within a single tile
</p>

<p>
These coordinates can be computed using <code>mod</code> directly or 
through the RManNotes convenience function called <i>repeat</i>. 
</p>

<table border=3 cellpadding=5 width=100%>
<tr><td>
<p align=right><small>RManNotes function</small></p>
<p>
<b>float repeat(float x, freq)</b>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<i>Repeats the parameter <code>x</code> over the interval 0 to 1
<code>freq</code> number of times. In other words, it 
returns the value of mod(x * freq, 1);
</i></p>
</table>

<h2><a name="cross">"Cross" Pattern</h2>

<p>
In the original <a href="transitions.html#cross_tile">crosstile shader</a>
which generates 1 tile, <i>s</i> and <i>t</i> were used to generate
a horizontal and a vertical bar.
</p>

<p align=center>
<img src="crosstile.gif"><br>
<a href="crosstile.sl">crosstile.sl</a>
</p>

<p>
By using <code>repeat</code> to compute <i>ss</i> and <i>tt</i> and
then using <i>ss</i> and <i>tt</i> instead of <i>s</i> and <i>t</i>
to generate the bars, the bars are repeated over the surface.
</p>

<p align=center>
<img src="cross.gif"><br>
<a href="cross.sl">cross.sl</a>
</p>

<h2><a name="which">Which Tile Are We Shading?</h2>

<p>
When <code>repeat</code> is used, each tile can be considered in the
same way since each tile will have the same 
<a href="#sstt">tile texture coordinates</a> (<code>ss</code> and <code>tt</code>
vary from 0 to 1 for every tile). In fact, one tile cannot 
be distinguished from another using <code>ss</code> and <code>tt</code>
alone. (That's why it's easy to make patterns using <code>repeat</code>.)
</p>

<p>
Sometimes however, it is useful to determine "which" tile
we are currently shading. For example, in a 
"brick" shader we might want to shift every other row horizontally 
by half a tile to give a staggered tiling. 
In this case, we need to know if the tile currently being shaded lies
in an odd or even row. Fortunately, computing the 
<b><i>tile coordinates</i></b> (the row and column) of the current tile
is easy using the <i>floor function</i>. 
</p>

<p>
In RManNotes, we'll use a function called <b><i>whichtile</i></b>
instead of using <code>floor</code> directly because it's easier to remember
and parallels the syntax of the <i>repeat function</i>.
</p>

<table border=3 cellpadding=5 width=100%>
<tr><td>
<p align=right><small>RManNotes function</small></p>
<p>
<b>float whichtile(float x, freq)</b>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<i>Returns the integer tile coordinate for the parameter <code>x</code>
which is being repeated <code>freq</code> times. This is computed
as floor(x * freq).
</i></p>
</table>

<p>
The code fragment below shows a typical usage of <code>whichtile</code>
with <code>repeat</code>. Note that the frequency passed to <code>whichtile</code>
should be the same as the frequency passed to <code>repeat</code> for the same dimension (<i>s</i> and/or <i>t</i>).

<pre>
  ss = repeat(s, sfreq);
  tt = repeat(t, tfreq);
  col = whichtile(s, sfreq);
  row = whichtile(t, tfreq);
</pre>
</p>

<p align=center>
<img src="which_tile.gif"><br>
"Tile coordinates" (<i>col,row</i>) over entire surface<br>
where the tile is repeated 4 times in both s and t.
</p>

<p>  
Two associated RManNotes convenience functions are 
<b><i>odd</i></b> and <b><i>even</i></b>.
</p>

<table border=3 cellpadding=5 width=100%>
<tr><td>
<p align=right><small>RManNotes function</small></p>
<p>
<b>boolean odd(float x)</b><br>
<b>boolean even(float x)</b><br>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<i><code>odd</code> returns true if <code>x</code> is an odd integer and
false otherwise.
Likewise, <code>even</code> returns true if <code>x</code> is an even integer
and false otherwise.
</i></p>
</table>

<p>
The <a href="#cross">cross</a> shader can be modified to produce a
<i>"weaving"</i> effect by alternating (by row and column) which bar is
on top -- the horizontal bar or the vertical bar.
</p>

<p align=center>
<img src="crossweave.gif"><br>
<a href="crossweave.sl">crossweave.sl</a>
</p>

<p><img src="black_line.gif"></p>
<p>
<a href="transitions.html">previous</a>
|
<a href="txtr_coords.html">next</a>
|
<a href="index.html">regular patterns: <b>contents</b></a>
|
<a href="../index.html">rmannotes: <b>top</b></a>
</i>
</p>

<p><img src="black_line.gif"></p> <small> <p><i>RManNotes</i> is Copyright &copy; 1995, 1996 Stephen F. May</p> <p> <i>Any comments or suggestions appreciated.</i> <address><a href="http://www.cgrg.ohio-state.edu/~smay">Steve May</a> (<a href="mailto:smay@cgrg.ohio-state.edu">smay@cgrg.ohio-state.edu</a>)</address> </p> </small>

<p align=right><small><i>Last Modified: 4/15/96</i></small></p>

</body>
</html>
