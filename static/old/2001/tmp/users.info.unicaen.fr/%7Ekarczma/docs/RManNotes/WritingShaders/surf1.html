<html>
<head>
<title>RManNotes: Writing Shaders: Surface</title>
</head>

<body bgcolor="#f9f9b1" background="notepad.gif" link="801010" vlink="101080" alink="108010"> <img alt="RManNotes" align=center src="header.gif">

<img align=center hspace=10 src="writing_shaders.gif">

<p><img src="black_line.gif"></p>
<p>
<a href="intro.html">previous</a>
|
<a href="surf2.html">next</a>
|
<a href="index.html">writing shaders: <b>contents</b></a>
|
<a href="../index.html">rmannotes: <b>top</b></a>
</i>
</p>
<p><img src="black_line.gif"></p>

<h1>Surface Shaders, Part I</h1>

<p>
This section describes some
<a href="#name">variable naming conventions</a>,
the 
<a href="#layers">construction and compositing of layers</a>,
and dealing with 
<a href="#transparency">transparency</a> in surface shaders.
<a href="surf2.html">Surface Shaders, Part II</a> discusses illumination
in surface shaders.
</p>

<h2><a name="vars">Variables</a></h2>

<p>
The table below summarizes local variables that we'll use by
convention in writing surface shaders.
</p>

<center>
<table border cellpadding=5>
<caption>Surface Shader Variables</caption>
<th align=left>name<th>type<th>description
<tr><td>surface_color<td>color<td>color of all composited layers
<tr><td>surface_opac<td>color<td>opacity of all composited layers
<br>(needed for transparent surfaces only)
<tr><td>layer_color<td>color<td>color of current layer
<tr><td>layer_opac<td>color<td>opacity of current layer
<tr><td>ss,tt<td>float<td>texture coordinates of current layer (2D)
<tr><td>PP<td>point<td>texture coordinates of current layer (3D)
</table>
</center>

<h2><a name="layers">Layers and Compositing</a></h2>

<p>
Most surface shaders will consist of multiple layers. The accumlated
color of all layers will be stored in a variable called
<code>surface_color</code>.
</p>

<p>
Initially, we'll assign a value directly to <code>surface_color</code> 
which will indicate the color of the background layer (layer 0). This
value may fixed by the shader-writer or definable by the user of the
shader using an instance variable or the global RSL variable <code>Cs</code>.
</p>

<p><pre><small>
	/* background layer (layer 0) */

	surface_color = color (0.3, 0.3, 0.3); /* grey */
</pre></small></p>

<p>
For each foreground layer, 
we'll use the variables <code>layer_color</code> and
<code>layer_opac</code> to represent the color and opacity for the layer
currently being defined (these variables will be reused for each layer). 
</p>

<p><pre><small>
	/* layer n */

	layer_color = color (0, 0, 1)             /* blue */
	layer_opac = pulse(0.35, 0.65, 0.02, s);  /* vertical stripe */
	surface_color = blend(surface_color, layer_color, layer_opac);
</pre></small></p>

<p>
Each layer will be composited on top of the previous layers
using the <code>blend()</code> function (which is just a vector version of
the RSL function <code>mix()</code>).
</p>

<table border=3 cellpadding=5 width=100%>
<tr><td>
<p align=right><small>RManNotes function</small></p>
<p>
<b>color blend(color a, color b, color x)</b>
</p>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<i>Performs a component-wise linear interpolation between <i>a</i>
and <i>b</i> based on the values in <i>x</i>. Note: blend() also
supports the argument types supported by mix().
</i></p>
</table>

<p>
The shader below shows two stripes composited on a background layer.
Note that the layer colors are constant and that the layer opacity 
specifies where the color will be applied for each layer. Also note
that layer #2 is 50% transparent and there are no illumination computations.
The shader is also available as <a href="surf1.1.sl">surf1.1.sl</a>.
</p>

<pre><small>
#include "rmannotes.sl"

surface surf1_1()
{
  color surface_color, layer_color;
  color layer_opac;

  /* background layer (layer 0) */

  surface_color = color (0.3, 0.3, 0.3); /* grey */

  /* layer #1 */
  
  layer_color = color (0,0,1);  /* blue */
  layer_opac = pulse(0.35, 0.65, 0.02, s);
  surface_color = blend(surface_color, layer_color, layer_opac);
  
  /* layer #2 */
  
  layer_color = color (0,1,0);  /* green */
  layer_opac = pulse(0.35, 0.65, 0.02, t);
  layer_opac *= 0.5;
  surface_color = blend(surface_color, layer_color, layer_opac);
  
  /* output */
  
  Ci = surface_color;
}
</pre></small>

<center>
<table>
<tr><td><img src="surf1.1a.gif"><td><img src="surf1.1b.gif"><td><img src="surf1.1c.gif"><td><img src="surf1.1d.gif">
<tr align=center><td>layer 0<td>layer 1<td>layer 2<td>all layers
</table>
</center>

<h2><a name="transparency">Transparent Surfaces</a></h2>

<p>
The shader above uses layers to determine surface color. In that shader,
the entire surface can be made <i>uniformly</i> transparent by setting the 
RSL global variable <code>Os</code> (if <code>Oi</code> is not assigned, 
<code>Oi = Os</code>).
</p>

<p>
A more sophisticated approach is to build up opacity using the 
individual layer opacities. This allows surfaces to be defined which
have varying opacity.
</p>

<p>
We will use the variable <code>surface_opac</code> to store the opacity
of the surface. When we use <code>surface_opac</code>, we'll assign
an initial value to it which will represent
the opacity of the background layer. For transparent surfaces, 
<code>surface_opac</code> should be initialized to a value less than 1.
It can be initialized using <code>Os</code>, an instance parameter, or
a fixed value.
</p>

<p><pre><small>
	/* background layer (layer 0) */

	surface_color = ...
	surface_opac = Os;                     /* user-defined */
</pre></small></p>

<p>
Compositing opacities is easy -- we just add
the opacity of the current layer to the overall surface opacity 
(stored in <code>surface_opac</code>) and clamp the surface opacity
to the range 0 to 1. The RManNotes 
<a href="../RegularPatterns/shapes.html#bool">union</a> 
function can be used for this purpose.
</p>

<p><pre><small>
	/* layer n */

	layer_color = ...
	layer_opac = ...
	surface_color = blend(...
	surface_opac = union(surface_opac, layer_opac);
</pre></small></p>

<p>
The shader <a href="surf1.2.sl">surf1.2.sl</a> is identical to
<a href="surf1.1.sl">surf1.1.sl</a> described above except that
the surface opacity varies based on the layers and <code>Cs</code>
and illumination has been added.
</p>

<center>
<table>
<tr><td><a href="surf1.2a.jpg"><img src="surf1.2a.gif"></a><td><a href="surf1.2b.jpg"><img src="surf1.2b.gif"></a>
<tr align=center><td>Os = (1,1,1)<td>Os = (0,0,0)
</table>
</center>


<p><img src="black_line.gif"></p>
<p>
<a href="intro.html">previous</a>
|
<a href="surf2.html">next</a>
|
<a href="index.html">writing shaders: <b>contents</b></a>
|
<a href="../index.html">rmannotes: <b>top</b></a>
</i>
</p>

<p><img src="black_line.gif"></p> <small> <p><i>RManNotes</i> is Copyright &copy; 1995, 1996 Stephen F. May</p> <p> <i>Any comments or suggestions appreciated.</i> <address><a href="http://www.cgrg.ohio-state.edu/~smay">Steve May</a> (<a href="mailto:smay@cgrg.ohio-state.edu">smay@cgrg.ohio-state.edu</a>)</address> </p> </small>

<p align=right><small><i>Last Modified: 5/8/97</i></small></p>

</body>
</html>
