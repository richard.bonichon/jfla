/* rect.sl
 *
 * generate rectangle with specified sides
 *
 */
#include "rmannotes.sl"

surface rect()
{
  color surface_color, layer_color;
  color layer_opac;
  float fuzz = 0.025;
  color red = color (1,0,0);
  float left, right, top, bottom;

  surface_color = Cs;

  layer_color = red;
  left = 0.05; right = 0.75; /* rectangle sides */
  top = 0.1; bottom = 0.7;
  layer_opac = pulse(left, right, fuzz, s) * pulse(top, bottom, fuzz, t);
  surface_color = blend(surface_color, layer_color, layer_opac);

  Ci = surface_color;
}
