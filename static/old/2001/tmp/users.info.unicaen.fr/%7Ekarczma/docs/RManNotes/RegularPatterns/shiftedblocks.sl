/* shiftedblocks.sl
 *
 * blocks with alternating rows shifted by 1/2 tile
 *
 */
#include "rmannotes.sl"

surface shiftedblocks()
{
  color surface_color, layer_color;
  color layer_opac;
  float ss, tt;
  float row;
  float fuzz = 0.05;
  float freq = 4;

  surface_color = Cs;

  /* shift even rows 1/2 tile */

  ss = repeat(s, freq);
  tt = repeat(t, freq);

  row = whichtile(t, freq);
  if (even(row))
    ss = mod(ss + 0.5, 1);

  /* squares */

  layer_color = color (0.3, 0.0, 0.3);
  layer_opac = intersection(pulse(0.35, 0.65, fuzz, ss), 
			    pulse(0.35, 0.65, fuzz, tt));
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* output */

  Ci = surface_color;
}
