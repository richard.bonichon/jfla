/* rotblocks.sl
 *
 * simple, rotated squares
 *
 */
#include "rmannotes.sl"

surface rotblocks()
{
  color surface_color, layer_color;
  color layer_opac;
  float ss, tt, tmps, tmpt;
  float col, row;
  float fuzz = 0.02;
  float freq = 4;

  /* init */

  surface_color = 0; /* black background */

  /* compute texture coords */

  tmps = repeat(s, freq);
  tmpt = repeat(t, freq);
  rotate2d(tmps, tmpt, radians(45), 0.5, 0.5, ss, tt);

  /* generate tile */

  layer_color = color (0.1, 1, 0.1);
  layer_opac = intersection(pulse(0.35, 0.65, fuzz, ss), 
			  pulse(0.35, 0.65, fuzz, tt));
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* output */

  Ci = surface_color;
}
