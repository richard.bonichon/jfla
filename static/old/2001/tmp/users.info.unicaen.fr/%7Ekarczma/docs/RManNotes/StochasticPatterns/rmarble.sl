/* rmarble.sl
 *
 * adapted from Pixar's distributed rmarble shader
 *
 */
#include "rmannotes.sl"

surface rmarble(float veining = 0.5;
		float Ks = 0.4, Kd = 0.6, Ka = 0.1, roughness= 0.1;
		color specularcolor = 1)
{
  color surface_color, layer_color;
  color layer_opac;
  point PP;
  point V, Nf ;
  float width, cutoff, fade, f, turb, maxfreq = 16;

  /* init */

  surface_color = 0;

  Nf = faceforward(normalize(N), I);
  V = -normalize(I);

  /* compute turbulence */

  PP = transform("shader", P) * veining;

  width = filterwidth_point(PP);
  cutoff = clamp(0.5 / width, 0, maxfreq);

  turb = 0;
  for (f = 1; f < 0.5 * cutoff; f *= 2) 
    turb += abs(snoise(PP * f)) / f;
  fade = clamp(2 * (cutoff - f) / cutoff, 0, 1);
  turb += fade * abs(snoise(PP * f)) / f;

  turb *= 0.5;  /* to match original rmarble turbulence value */

  /* use turb to index into spline for layer color */

  layer_color = spline(turb,
		       color (0.8, 0.2, 0.05),
		       color (0.8, 0.2, 0.05),
		       color (0.8, 0.5, 0.3),
		       color (0.6, 0.594, 0.58),
		       color (0.3, 0.3, 0.4),
		       color (0.05, 0.05, 0.1),
		       color (0.8, 0.8, 0.79),
		       color (0.8, 0.8, 0.79)); 
  layer_opac = 1;
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* plastic illum for all layers */

  surface_color = surface_color * (Ka * ambient() + Kd * diffuse(Nf)) +
    specularcolor * Ks * specular(Nf, V, roughness);

  /* output */

  Oi = Os;
  Ci = Os * surface_color;
}
