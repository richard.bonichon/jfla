/* rings.sl
 *
 * repeated ring pattern 
 *
 */
#include "rmannotes.sl"

surface rings()
{
  color surface_color, layer_color;
  color surface_opac, layer_opac;
  float ss, tt;
  point center;
  float radius, width;
  float d;
  float fuzz = 0.025;
  float freq = 4;
  color ringcolor = color (1,0,0);

  /* init */

  surface_color = Cs;
  surface_opac = Os;

  /* repeated rings */

  ss = repeat(s, freq);
  tt = repeat(t, freq);

  center = (0.5, 0.5, 0);  /* position of ring */
  radius = 0.35;           /* radius of ring */
  width = 0.1;             /* width of ring */
  d = distance(center, (ss, tt, 0));

  layer_color = ringcolor;
  layer_opac = pulse(radius - width / 2, radius + width / 2, fuzz, d);
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* output */

  Oi = surface_opac;
  Ci = surface_opac * surface_color;
}
