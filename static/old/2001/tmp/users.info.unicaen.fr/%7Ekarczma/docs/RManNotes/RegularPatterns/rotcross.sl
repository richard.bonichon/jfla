/* rotcross.sl
 *
 * rotates & repeats a simple cross pattern a fixed number of times
 *
 */
#include "rmannotes.sl"

surface rotcross()
{
  color surface_color, layer_color;
  color surface_opac, layer_opac;
  float fuzz = 0.05;
  float ss, tt;
  float freq = 4;

  /* background layer */

  surface_color = Cs;
  surface_opac = Os;

  /* rotate 45 degrees and 
     repeat pattern 'freq' times horizontally & vertically */

  rotate2d(s, t, radians(45), 0.5, 0.5, ss, tt);
  ss = repeat(ss, freq);
  tt = repeat(tt, freq);

  /* vertical bar layer */

  layer_color = color (0.1, 0.5, 0.1);
  layer_opac = pulse(0.35, 0.65, fuzz, ss);
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* horizontal bar layer */

  layer_color = color (0.1, 0.1, 0.3);
  layer_opac = pulse(0.35, 0.65, fuzz, tt);
  surface_color = blend(surface_color, layer_color, layer_opac);

  /* output */

  Oi = surface_opac;
  Ci = surface_opac * surface_color;
}
