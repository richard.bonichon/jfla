<html>
<head>
<title>BMRT Documentation</title>
</head>
<body>

<center>
<h1>Blue Moon Rendering Tools 2.4
<p>Part 4: Previewing RIB files with <em>rgl</em></h1>
<h3>

<h1><br></h1>

<p>revised 27 August 1998
<p>
<h1><br></h1>

<p>Larry I. Gritz
<br>
gritzl@acm.org
<br>
</h3></center>

<h1><br></h1>

<hr>


<p>Once a RIB file is created, one may use the <em>rgl</em> program to
display a preview of the scene.  Geometric primitives are displayed
either as Gouraud-shaded polygons with simple shading and hidden
surface removal performed, or as a wireframe image.

<p> The following command will display a preview of the animation in
an OpenGL window:

<pre>
         rgl myfile.rib
</pre>

There are several command line options which can be given (listed in
any order, but prior to the filename).  The following sections
describe these options.  The different options may be used together
when they are not inherently contradictory.

<p>If no filename is specified to <em>rgl</em>, it will attempt to
read RIB from standard input (stdin).  This allows you to pipe output
of a RIB process directly to <em>rgl</em>.  For example, suppose that
myprog dumps RIB to its standard output.  Then you could display RIB
frames from myprog as they are being generated with the following
command:

<pre>
         myprog | rgl
</pre>

The RIB file which you specify may contain either a single frame or
multiple frames (if it is an animation sequence).  The <em>rgl</em>
program is designed primarily for previewing animation sequences of
many RIB frames.  The default is to display all of the frames
specified in the RIB file as quickly as possible.

<p>When the last frame is displayed, it will remain in the window.  If
you hit the ESC key (with the mouse in the drawing window),
<em>rgl</em> will terminate.  If you click on the window with the left
mouse button, the entire RIB sequence will be played again.

<p>Though the output of <em>rgl</em> is in color, it is important to
note that it is not designed to be a particularly accurate preview of
a rendered image.  It really cannot be, since there is no way for
<em>rgl</em> to know very much about the types of shaders which you
are using.  It does a fairly good job of matching ambient, point,
distant, and spot lights.  But it can't figure out area lights or any
nonstandard light source types.  Also, every surface is displayed as
if it were "matte", regardless of the actual surface specification.

Note that <em>rgl</em> can also display primitives as lines.  This
is done by invoking:

<pre>
        rgl -lines myfile.rib
</pre>

<p>This completely replaces the old <em>rendribv</em> program.
<p>


<h3>4.1 Image Generation and File Options</h3>

<p>The following subsection details command line options alter the way
in which <em>rgl</em> creates and/or displays images.

<h4>4.1.1 Setting Image Resolution</h4>

<p>
<dl>
<dt><b><code>-res</code> <em>xres yres</em></b>
<dd>The default resolution of the window in which the preview is
displayed is 640 horizontal pixels by 480 vertical pixels.  This
resolution is used when no explicit <code>Format</code> statement is
given in the RIB file.  If you want a different size window, you can
override the default by using the <code>-res</code> command line
argument.  For example, the following will display the myfile.rib file
in a 320 x 240 pixel window:

<pre>
	rgl -res 320 240 myfile.rib
</pre>

<p>The <code>-res</code> option requires two numerical arguments to
specify the horizontal and vertical resolutions, respectively.  Note
that if the RIB file contains a <code>Format</code> statement which explicitly
specifies the image resolution, then the <code>-res</code> option will
be ignored and the window will be opened with the resolution specified
in the <code>Format</code> statement.
</dl>

<h4>4.1.2 Drawing Styles</h4>

<p>
<dl>
<dt><code><b>-1buffer</b></code>
<dd>Rather than render the polygon preview to the "back buffer" and
displaying frames as they finish (as you would want especially if you
are previewing an animation), this option draws to the front buffer, thus
allowing you to see the scene as rendering progresses.
The <code>-1buffer</code>
option may be used in combination with any of the other drawing style
options.

<dt><code><b>-unlit</b></code>
<dd>Lights all geometry with a single light at the camera position.
     This is useful for using <em>rgl</em> to preview a RIB file that
     does not contain light sources.  The <code>-unlit</code>
option may be used in combination with any of the other drawing style
options.

<dt><code><b>-lines</b></code>
<dd>Rather than the default drawing mode of filled-in Gouraud-shaded
     polygons, this option causes the images to be rendered as
     lines.  Note that this cannot be used in combination with
     <code>-sketch</code>.

<dt><code><b>-sketch</b></code>
<dd>It's not clear what the real use of this is, but it makes an
     image that looks a little like a human-drawn sketch of the
     objects.  Note that this cannot be used in combination with
     <code>-lines</code>.

</dl>


<h4>4.1.3 File Output Options</h4>

<p>
<dl>
<dt><code><b>-dumprgba -dumprgbaz</b> </code>
<dd>The default operation of <em>rgl</em> simply previews the scene to
a window on your display.  But using the <code>-dumprgba</code> option
instead causes the resulting preview image to be saved to a TIFF file.
The filename of the TIFF file is taken from the <code>Display</code>
RIB command in the file itself, or <code>ri.tif</code> if no
<code>Display</code> command is present in the RIB file.  The
<code>-dumprgbaz</code> option does the same thing as
<code>-dumprgba</code>, but also saves the z buffer values to a file.
The z values are saved in the same zfile format used by Pixar's
<em>PhotoRealistic RenderMan</em>, and the name of the file is also
taken from the <code>Display</code> RIB command, substituting "zfile"
for "tif" in the filename.

<dt><code><b>-offscreen</b></code>
<dd>When used in conjunction with either <code>-dumprgba</code> or
<code>-dumprgbaz</code>, causes the image file(s) to be created
without ever opening a window to the screen.  This is handy for
using rgl as a low quality batch renderer.

</dl>



<h3>4.2 Previewing Animations</h3>

<h4>4.2.1 Displaying a Subset of Frames</h4>

<p>Sometimes you may only want to preview a subset of frames from a
multi-frame RIB file.  You can do this by using the
<code>-frames</code> command line option.  This option takes two
integer arguments: the first and last frame numbers to display.  For
example,

<pre>
	rgl -frames 10 20 myfile.rib
</pre>

<p>This example will preview only frame numbers 10 through 20.  If you
are going to use this option, it is recommended that your frames be
numbered sequentially starting with 0 or 1.

<h4>4.2.2 Synchronizing to the Clock</h4>

<p>When previewing a series of frames for an animation, it is often
necessary to synchronize the display of frames to the clock in order
to check the timing of the animation when it is played back at a
particular number of frames per second.  The default action of
<em>rgl</em> is to display the frames as fast as possible.  You can
override this, causing <em>rgl</em> to try to display a particular
number of frames per second, by using the <code>-sync</code> command
line option.  The following example displays a RIB file as closely as
possible to 30 frames per second:

<pre>
	rgl -sync 30 myfile.rib
</pre>

<h4>4.2.3 No Pausing for keyboard input</h4>

<p>By default, the last frame will stay in the drawing window until
you hit the ESC key.  Sometimes you may wish <em>rgl</em> to terminate
immediately after displaying the last frame in the sequence (for
example, if it is part of an automated demo). You can use the -nowait
command line option to fix this:

<pre>
	rgl -nowait myfile.rib
</pre>

This command will open the window, display all frames in the RIB file,
and terminate immediately without waiting for you to hit the ESC key.


<h3>4.3 Geometric Approximation</h3>

<p>Curved surface primitives are displayed by <em>rgl</em> as grids of
polygons.  You can speed up
<em>rgl</em> by changing the refinement detail that it uses to convert
curved surfaces to polygons by using the <code>-rd</code> command line
option.  This option takes a single numerical argument, generally
between 0 and 1.  For example:

<pre>
	rgl -sync 30 -rd 0.5 myfile.rib
</pre>

 The lower the value, the fewer polygons will be used to approximate
curved surfaces.  Using a value of 1 will result in identical results
as if you did not use the <code>-rd</code> option at all.  Good values
to try are 0.75 and 0.5.  If you go below 0.25, the curved surface
primitives may become unrecognizable, though they will certainly be
drawn quickly.  If you use values larger than 1, even more polygons
than usual will be used to approximate the curved surfaces.

<p>IMPORTANT NOTE: the <code>-rd</code> option can only speed up the
rendering of curved surface primitives (e.g. spheres, cylinders,
bicubic patches, NURBS).  It WILL NOT speed up the drawing of polygons.  If
your model contains too many polygons to be drawn quickly, the
<code>-rd</code> option will not help you.



<h3>4.4 Limitations of <em>rgl</em></h3>

<p>Since <em>rgl</em> is an OpenGL-based polygon previewer, it cannot
possibly support all the features of the RenderMan Interface which
would be supported by other types of renders.  This section outlines
the features which are not fully supported by <em>rgl</em>.

<ol>

<li> The following are ignored: ColorSamples, CropWindow,
DepthOfField, Shutter, PixelVariance, PixelSamples, PixelFilter,
Exposure, Imager, Quantize, Display, Hider, Atmosphere, Bound,
Opacity, TextureCoordinates, ShadingRate, ShadingInterpolation, Matte.

<li> LightSource works as expected for "ambientlight", "distantlight"
and "pointlight".  It isn't smart enough to know exactly what to do
for custom light source shaders, but it will try to make its best
guess by examining the parameters to the shader, looking for clues
like "from", "to", "lightcolor", and so on.  AreaLightSource has no
effect.

<li> Shaders do nothing.  All surfaces are displayed as if they were "matte".

<li> When motion blocks are given, only the first time key is used.

<li> Multiple levels of detail are not supported.

<li> Solids are all displayed as unions, i.e. all of the components of
a CSG primitive are displayed.

<li> Object instancing is not currently working.  Instanced objects
are ignored.

<li> Texture mapping functions (e.g. MakeTexture) do nothing in <em>rgl</em>.
</ol>



<hr>


<p>This document last updated 27 Aug 98 by <a href="mail:gritzl@acm.org">gritzl@acm.org</a>

<p>All BMRT components are Copyright 1990-1998 by Larry I. Gritz.
All rights reserved.

<p>The RenderMan Interface Procedures and RIB Protocol are:
Copyright 1988, 1989, Pixar.  All rights reserved.
RenderMan is a registered trademark of Pixar.

</body>
</html>
