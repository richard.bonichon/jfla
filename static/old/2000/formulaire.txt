                          FORMULAIRE D'INSCRIPTION

                             � retourner �:

                          INRIA Rocquencourt
                          Relations Ext�rieures - JFLA'2000
                          Domaine de Voluceau - BP 105
                          78153 LE CHESNAY Cedex (France)
 
                            avant le 15 janvier 2000


Journ�es Francophones des Langages Applicatifs

Mont Saint Michel (France) 31 janvier - 1er f�vrier 2000

Nom : ..........................................................................
Pr�nom : .......................................................................

Organisme : ....................................................................

Adresse : ......................................................................
Code Postal : ..................................................................
Ville : ........................................................................
Pays : .........................................................................
T�l : ..........................................................................
T�l�copie : ....................................................................
E-mail : .......................................................................


Droits d'inscription (TVA incluse):

                

Tarif participant
- en chambre individuelle       2 000,00 FF (304,90 euros TTC.) 
- en chambre double             1 500,00 FF (228,67 euros TTC.) 

Votre tarif : ..................................................................

Paiement :
Les r�glements s'effectueront en Francs Fran�ais � l'ordre de l'Agent
Comptable de l'INRIA

- PAR BON DE COMMANDE
Veuillez trouver ci-joint le bon de commande de l'organisme payeur

- PAR CHEQUE BANCAIRE
Veuillez trouver ci-joint un ch�que de /__/__/__/__/,__/__/ FF

- PAR VIREMENT BANCAIRE A:
Tr�sorerie G�n�rale des Yvelines, 16 avenue de Saint-Cloud, 78018
Versailles Cedex (France).
Compte N� 10071 - 78000 - 00003003958 - 80
(Code banque : 10071 ; guichet : 78000 ; num�ro de compte : 00003003958 ;
Cl� : 80)
Bien mentionner sur ces documents votre NOM et la r�f�rence JFLA2000


Date d'arriv�e � l'h�tel :
|_| Samedi 29 janvier                     |_| Dimanche 30 janvier             

Heure approximative d'arriv�e :.................................................

Date de d�part de l'h�tel :
|_| Mardi 1er f�vrier                     |_| Mercredi 2 f�vrier

Heure de d�part pr�vue : .......................................................

Navette Gare-conf�rence

Je suis int�ress�(e) par la navette du 31 janvier 2000 d�part gare de
Rennes � 17h               oui |_|  non |_| 

Je suis int�ress�(e) par la navette du 1er f�vrier 2000 d�part Mont
Saint Michel � 16h30.      oui |_|  non |_|


Excursion

Je d�sire participer � l'excursion du mardi matin (50FF �  r�gler sur place)
                           oui |_|  non |_|

R�gime alimentaire particulier

Je suis un r�gime alimentaire particulier 
                           oui |_|  non |_|

Annulation

Le remboursement des frais d'inscription sera possible pour toute demande
�crite parvenant au secr�tariat 8 jours avant le d�but des Journ�es (cachet
de la poste faisant foi). Aucun remboursement ne sera effectu� au-del� de
cette date.

        Date :        Signature



        ........      .........

Renseignements

Florence Balax
INRIA Rocquencourt
Relations Ext�rieures - JFLA'2000
Domaine de Voluceau - BP 105
78153 Le Chesnay Cedex
T�l. : +33 (0)1 39 63 56 00 - T�l�copie : +33 (0)1 39 63 56 38
Email : symposia@inria.fr
http://pauillac.inria.fr/jfla/2000/
