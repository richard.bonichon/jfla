* MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER *

DEUXIEME APPEL AUX COMMUNICATIONS       DEUXIEME APPEL AUX COMMUNICATIONS

                        JFLA'2010 (http://jfla.inria.fr/2010/)

               Journ�es Francophones des Langages Applicatifs
                        Organis�es par l'INRIA

                        30 janvier au 2 f�vrier 2010

JFLA'2010 est la vingt et uni�me conf�rence francophone organis�e autour des
langages applicatifs et des techniques de certification bas�es sur la
d�monstration.

Ces nouvelles journ�es se tiendront du

            30 janvier au 2 f�vrier 2010.

Elles auront lieu � la mer, �

            Vieux-Port La Ciotat, � proximit� de Marseille.

Toujours centr�e sur l'approche fonctionnelle de la programmation, la
conf�rence porte �galement sur les techniques et outils compl�mentaires qui
�l�vent le niveau de qualit� des logiciels (syst�mes d'aide � la preuve,
r��criture, tests, d�monstration automatique, v�rification).

Les JFLA r�unissent concepteurs et utilisateurs dans un cadre
agr�able qui facilite la communication; ces journ�es ont pour ambition de
couvrir le domaine des langages applicatifs au sens large, en y incluant les
apports d'outils d'autres domaines qui permettent la construction de syst�mes
logiciels plus s�rs. L'enseignement de l'approche fonctionnelle du
d�veloppement logiciel (sp�cification, s�mantiques, programmation, compilation,
certification) est �galement un sujet qui concerne au plus haut point les
JFLA.

C'est pourquoi des contributions sur les th�mes suivants sont
particuli�rement recherch�es (liste non exclusive) :

- Langages fonctionnels et applicatifs : s�mantique, compilation, optimisation,
  mesures, tests, extensions par d'autres paradigmes de programmation.

- Sp�cification, prototypage, d�veloppements formels d'algorithmes.

- Utilisation industrielle des langages fonctionnels et applicatifs.

- Assistants de preuve : impl�mentation, nouvelles tactiques,
  d�veloppements pr�sentant un int�r�t technique ou m�thodologique.

- Enseignement dans ses aspects li�s � l'approche fonctionnelle
  du d�veloppement.

Les JFLA cherchent avant tout des articles de recherche originaux qui apportent
une r�elle nouveaut�. Toutefois, un article traitant d'un sujet qui int�resse
plusieurs disciplines sera examin� avec soin, m�me s'il a pr�alablement �t�
pr�sent� � une autre communaut� sans rapport avec celle des JFLA.
Un article ayant �t� traduit en fran�ais � partir d'une publication r�cente en
anglais sera examin�, � condition que la traduction apporte un �l�ment
nouveau.

Les articles soumis aux JFLA sont relus par au moins 2 personnes s'ils sont
accept�s, 3 personnes s'ils sont rejet�s.

Les critiques des relecteurs sont toujours bienveillantes et la plupart du
temps encourageantes et constructives, m�me en cas de rejet.

Il n'y a donc pas de raison de ne pas soumettre aux JFLA !

Orateurs invit�s
----------------
 Leslie Lamport (Microsoft): � Preuves et prouveur TLA+ �
 Christian Queinnec (Universit� Paris 6): � De la correction automatis�e -- 
<A HREF="actes/PRESENTATIONS/invite-queinnec.pdf">pr�sentation</A> �


Cours
-----
 Louis Mandel (Universit� Paris 11): � Cours de ReactiveML -- 
<A HREF="actes/PRESENTATIONS/cours-mandel.pdf">pr�sentation 1</A>
<A HREF="actes/PRESENTATIONS/cours-mandel2.pdf">pr�sentation 2</A> �
 Pierre Letouzey (Universit� paris 7): � De Coq � ML : l'extraction de programmes -- 
<A HREF="actes/PRESENTATIONS/cours-letouzey.pdf">pr�sentation</A> �

Comit� de programme
-------------------
        Micaela Mayero, Pr�sidente (Universit� Paris 13)

        Sylvain Conchon, Vice Pr�sident (Universit� Paris 11)

        Assia Mahboubi (INRIA Saclay)

        Alan Schmitt (INRIA Grenoble - Rh�ne-Alpes)

        Pierre Courtieu (CNAM Paris)

        Damien Pous (CNRS Grenoble)

        Bruno Barras (INRIA Saclay)

        Bernard Serpette (INRIA Sophia Antipolis - M�diterran�e)

        Manuel Serrano (INRIA Sophia Antipolis - M�diterran�e)

        Damien Doligez (INRIA Paris - Rocquencourt)

Soumission
----------
Date limite de soumission : extension au 22 octobre 2009

Les actes devraient �tre publi�s soit au format LNCS
chez Springer, soit dans le journal �Studia Informatica
Universalis� chez Hermann.


Les soumissions doivent �tre soit r�dig�es en fran�ais, soit
pr�sent�es en fran�ais. Elles sont limit�es � 15 pages A4. Le style
latex est impos� et se trouve sur le site WEB des journ�es � l'adresse
suivante :

         http://jfla.inria.fr/2010/actes.sty

La soumission est uniquement �lectronique, selon la m�thode d�taill�e dans

         http://jfla.inria.fr/2010/instructions.fr.html

Les soumissions sont � envoyer au pr�sident du comit� de programme,
avec pour titre de votre message ``SOUMISSION JFLA 2010'', � l'adresse
suivante :

             Micaela.Mayero [@] lipn.univ-paris13.fr

Les intentions de soumission envoy�es le plus t�t possible � l'adresse
ci-dessus seront les bienvenues.


Dates importantes
-----------------
extension au 22 octobre 2009 : Date limite de soumission
20 novembre (repouss�e au 25 novembre) 2009 : Notification aux auteurs
10 d�cembre 2009 : Remise des articles d�finitifs
15 janvier 2010 : Date limite d'inscription aux journ�es
30 janvier au 2 f�vrier 2010 : Journ�es

Pour tout renseignement, contacter
----------------------------------

INRIA Grenoble Rh�ne-Alpes
Bureau des Cours-Colloques
655 avenue de l'Europe - Montbonnot
38334 Saint Ismier Cedex - France

Tel : + 33 (0)4 76 61 52 23 - Fax : + 33 (0)4 76 61 52 06
email : colloques@inrialpes.fr

http://jfla.inria.fr/2010/
