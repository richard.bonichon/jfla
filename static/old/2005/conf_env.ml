(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2004 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

(* The year when we are compiling this program! *)
let this_year = 2004;;
(* The year when the conference will hold. *)
let year_conf = 2005;;

let nieme_conf = "seizi�me";;

(* Dead-lines. *)
let date_limite_soumission = "8 octobre";;
let date_notification = "27 novembre";;
let date_remise_article = "09 janvier";;
let date_limite_inscription = "01 f�vrier";;

(* Dates of the meeting. *)
let date_conf = "9 et 10 mars";;
let premier_jour_conf = "9 mars";;
let deuxieme_jour_conf = "10 mars";;
let date_arrivee_conf = "mardi 8 mars";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "Olivier Michel";;
let mail_president_comite = "michel@lami.univ-evry.fr";;
let institution_president_comite =
 "LAMI, Universit� �vry-Val d'Essonne, �vry";;
let mail_soumission = "jfla2005@lami.univ-evry.fr";;

(* Lee membres du comit� de programme. *)
let membre_comite1 = "Catherine Dubois";;
let mail_membre_comite1 = "dubois@iie.cnam.fr";;
let institution_membre_comite1 = "CEDRIC, Institut d'Informatique d'Entreprise/CNAM";;

let membre_comite2 = "Jean-Pierre Gallois";;
let mail_membre_comite2 = "GALLOIS@ortolan.cea.fr";;
let institution_membre_comite2 = "projet AGATHA/CEA Saclay";;

let membre_comite3 = "Jacques Garrigue";;
let mail_membre_comite3 = "garrigue@kurims.kyoto-u.ac.jp";;
let institution_membre_comite3 = "Universit� de Kyoto";;

let membre_comite4 = "Frederic Loulergue";;
let mail_membre_comite4 = "loulergue@univ-paris12.fr";;
let institution_membre_comite4 = "LACL, Universit� de Paris-XII Cr�teil";;

let membre_comite5 = "Pierre-Etienne Moreau";;
let mail_membre_comite5 = "Pierre-Etienne.Moreau@loria.fr";;
let institution_membre_comite5 = "projet  PROTHEO, LORIA";;

let membre_comite6 = "Lo�c Pottier";;
let mail_membre_comite6 = "loic.pottier@sophia.inria.fr";;
let institution_membre_comite6 = "projet LEMME, INRIA Sophia-Antipolis";;

let membre_comite7 = "Manuel Serrano";;
let mail_membre_comite7 = "Manuel.Serrano@sophia.inria.fr";;
let institution_membre_comite7 = "projet MIMOSA, INRIA Sophia-Antipolis";;

let membre_comite8 = "Patrick Viry";;
let mail_membre_comite8 = "pviry@ilog.fr";;
let institution_membre_comite8 = "ILOG";;

let membre_comite9 = "";;
let mail_membre_comite9 = "";;
let institution_membre_comite9 = "";;

(* Les conf�renciers invit�s. *)
let orateur_invite_1 = "Inconnu";;
let institution_orateur_invite_1 = "";;
let titre_conf_invite_1 = "(Non communiqu�)";;

let orateur_invite_2 = "Inconnu";;
let institution_orateur_invite_2 = "";;
let titre_conf_invite_2 =
  "(Non communiqu�)";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
let ou_conf = "� la montagne";;
let lieu_conf = "Berne (Suisse)";;
let gare_conf = "(Non communiqu�)";;
let aeroport_conf = "(Non communiqu�)";;
let distance_aeroport_conf = "(Non communiqu�)";;
let autre_aeroport_conf = "";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "(Non communiqu�)";;
let horaires_train_aller_paris = "(Non communiqu�)";;
let horaires_train_retour_paris = "(Non communiqu�)";;
let aeroport_paris = "(Non communiqu�)";;
let distance_de_bordeaux = "(Non communiqu�)";;
let distance_de_lyon = "(Non communiqu�)";;
let distance_de_marseille = "(Non communiqu�)";;
let distance_de_nantes = "(Non communiqu�)";;
let distance_de_paris = "(Non communiqu�)";;
let distance_de_poitiers = "(Non communiqu�)";;
let approche_conf = "(Non Communiqu�)
";;

(* Navette et h�tel. *)
let heure_navette_arrivee = "17h30";;
let heure_navette_depart = "15h30";;
let hotel_conf = "� Inconnu �";;
let web_hotel_conf = "(non communiqu�)";;
let adresse_hotel_conf =
  printf__sprintf "\
<BLOCKQUOTE>
%s<BR>
(adresse)<BR>
Tel: <BR>
Fax: <BR>
%s
</BLOCKQUOTE>
" hotel_conf web_hotel_conf;;

let pub_site_conf =
  "<IMG SRC=\"pub_site_conf.jpg\" ALT=\"Site de la manifestation\">";;

let montant_chambre_simple = "230";;
let montant_chambre_double = "190";;
let montant_devise = "euros";;

let social_event_conf = "une balade � pied ou en v�lo";;
let date_social_event_conf = "lundi " ^ premier_jour_conf ^ " apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence. *)
let presentation_1 = "Ocaml-templates, g�n�ration de code � partir des types";;
let auteurs_1 = "Fran�ois Maurel (PPS, Paris 7)";;
                                                                               
let presentation_2 =
  "Typage fort et typage souple des collections topologiques";;
let auteurs_2 = "Julien Cohen (LAMI, �vry)";;
 
let presentation_3 =
  "Une biblioth�que certifi�e de programmes fonctionnels BSP";;
let auteurs_3 = "Fr�d�ric Gava (LACL, Paris 12)";;
 
let presentation_4 =
  "Une proc�dure de d�cision r�flexive pour un fragment \
   de l'arithm�tique de Presburger";;
let auteurs_4 = "Pierre Cr�gut (France-T�l�com R & D - DTL/TAL)";;

let presentation_5 = "Gb: une proc�dure de d�cision pour le syst�me Coq";;
let auteurs_5 =
  "J�r�me Cr�ci (Saarbrucken), Lo�c Pottier (INRIA Sophia-Antipolis)";;
 
let presentation_6 = "Application du toplevel embarqu� d'Objective Caml";;
let auteurs_6 =
  "Cl�ment Capel, Emmanuel Chailloux (PPS, Paris 7), \
   Jean-Marc Eber (LexiFi SAS)";;
 
let presentation_7 = "GlSurf: maths et dessins en Ocaml";;
let auteurs_7 =
  "Christophe Raffalli (Laboratoire de Math�matiques, Univ. Savoie)";;
 
let presentation_8 =
  "Formalisation en Coq d'un cours de g�om�trie pour le lyc�e";;
let auteurs_8 = "Fr�d�rique Guilhot (INRIA Sophia-Antipolis)";;
 
let presentation_9 =
  "�valuation de l'extensibilit� de PhoX: B/PhoX \
   un assistant de preuves pour B";;
let auteurs_9 =
  "J�r�me Rocheteau, Samuel Colin, Georges Mariano, Vincent Poirriez";;
 
let presentation_10 =
  "Algorithmes et complexit�s de la r�duction statique minimale";;
let auteurs_10 = "Ludovic Henrio, Bernard Paul Serpette, Szabolcs Szentes";;
 
let presentation_11 =
  "Programmation param�tr�e en ML et automates d'architecture";;
let auteurs_11 = "Philippe Narbel (LaBRI, Bordeaux I)";;

let titre_table_ronde =
  "La place et la pratique du test dans la programmation fonctionnelle";;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)

(* Fields to fill to generate the Web site. *)
let nom_conf = "jfla";;
let upper_nom_conf = "JFLA";;

(* Fin des donn�es d�pendantes de la conf�rence. *)

let date_depart_conf = deuxieme_jour_conf;;

(* Administration des inscriptions. *)
let contact_administratif = "Marie-Fran�oise Loubressac";;
let mail_contact_administratif = "Marie-Francoise.Loubressac@inria.fr";;
let telephone_administratif =
  "T�l.: +33 (0) 1 39 63 56 00 - Fax : +33 (0) 1 39 63 56 38";;
let mail_reception_inscriptions = "symposia@inria.fr";;

(* Emplacement des sites Webs. *)

let http_root = "http://pauillac.inria.fr";; 
let http_conf_site_root = http_root ^ "/" ^ nom_conf;; 

let info_touristiques = "\
<UL>
  <LI><A HREF=\"" ^ web_hotel_conf ^ "\"></A>
    ou Office du tourisme de $(LIEU_CONF).
</UL>\
";;

let conf_call_back = "treat_form";;

let last_year_conf = year_conf - 1;;
let next_year_conf = year_conf + 1;;

let year_conf_str = string_of_int year_conf;;

let env = [
  "NOM_CONF", nom_conf;
  "UPPER_NOM_CONF", upper_nom_conf;

  "YEAR_CONF", year_conf_str;
  "THIS_YEAR", string_of_int this_year;
  "LAST_YEAR_CONF", string_of_int last_year_conf;
  "NEXT_YEAR_CONF", string_of_int next_year_conf;
  "NIEME_CONF", nieme_conf;
  "DATE_ARRIVEE_CONF", date_arrivee_conf;
  "DATE_DEPART_CONF", date_depart_conf;
  "PREMIER_JOUR_CONF", premier_jour_conf;
  "DEUXIEME_JOUR_CONF", deuxieme_jour_conf;
  "DATE_CONF", date_conf;

  "DATE_LIMITE_SOUMISSION", date_limite_soumission;
  "DATE_NOTIFICATION", date_notification;
  "DATE_REMISE_ARTICLE", date_remise_article;
  "DATE_LIMITE_INSCRIPTION", date_limite_inscription;

  "PRESIDENT_COMITE", president_comite;
  "MAIL_PRESIDENT_COMITE", mail_president_comite;
  "MAIL_SOUMISSION", mail_soumission;
  "INSTITUTION_PRESIDENT_COMITE", institution_president_comite;

  "MEMBRE_COMITE1", membre_comite1;
  "MAIL_MEMBRE_COMITE1", mail_membre_comite1;
  "INSTITUTION_MEMBRE_COMITE1", institution_membre_comite1;

  "MEMBRE_COMITE2", membre_comite2;
  "MAIL_MEMBRE_COMITE2", mail_membre_comite2;
  "INSTITUTION_MEMBRE_COMITE2", institution_membre_comite2;

  "MEMBRE_COMITE3", membre_comite3;
  "MAIL_MEMBRE_COMITE3", mail_membre_comite3;
  "INSTITUTION_MEMBRE_COMITE3", institution_membre_comite3;

  "MEMBRE_COMITE4", membre_comite4;
  "MAIL_MEMBRE_COMITE4", mail_membre_comite4;
  "INSTITUTION_MEMBRE_COMITE4", institution_membre_comite4;

  "MEMBRE_COMITE5", membre_comite5;
  "MAIL_MEMBRE_COMITE5", mail_membre_comite5;
  "INSTITUTION_MEMBRE_COMITE5", institution_membre_comite5;

  "MEMBRE_COMITE6", membre_comite6;
  "MAIL_MEMBRE_COMITE6", mail_membre_comite6;
  "INSTITUTION_MEMBRE_COMITE6", institution_membre_comite6;

  "MEMBRE_COMITE7", membre_comite7;
  "MAIL_MEMBRE_COMITE7", mail_membre_comite7;
  "INSTITUTION_MEMBRE_COMITE7", institution_membre_comite7;

  "MEMBRE_COMITE8", membre_comite8;
  "MAIL_MEMBRE_COMITE8", mail_membre_comite8;
  "INSTITUTION_MEMBRE_COMITE8", institution_membre_comite8;

  "MEMBRE_COMITE9", membre_comite9;
  "MAIL_MEMBRE_COMITE9", mail_membre_comite9;
  "INSTITUTION_MEMBRE_COMITE9", institution_membre_comite9;

  "ORATEUR_INVITE_1", orateur_invite_1;
  "INSTITUTION_ORATEUR_INVITE_1", institution_orateur_invite_1;
  "TITRE_CONF_INVITE_1", titre_conf_invite_1;
  "ORATEUR_INVITE_2", orateur_invite_2;
  "INSTITUTION_ORATEUR_INVITE_2", institution_orateur_invite_2;
  "TITRE_CONF_INVITE_2", titre_conf_invite_2;

  "PRESENTATION_1", presentation_1;
  "AUTEURS_1", auteurs_1;
  "PRESENTATION_2", presentation_2;
  "AUTEURS_2", auteurs_2;
  "PRESENTATION_3", presentation_3;
  "AUTEURS_3", auteurs_3;
  "PRESENTATION_4", presentation_4;
  "AUTEURS_4", auteurs_4;
  "PRESENTATION_5", presentation_5;
  "AUTEURS_5", auteurs_5;
  "PRESENTATION_6", presentation_6;
  "AUTEURS_6", auteurs_6;
  "PRESENTATION_7", presentation_7;
  "AUTEURS_7", auteurs_7;
  "PRESENTATION_8", presentation_8;
  "AUTEURS_8", auteurs_8;
  "PRESENTATION_9", presentation_9;
  "AUTEURS_9", auteurs_9;
  "PRESENTATION_10", presentation_10;
  "AUTEURS_10", auteurs_10;
  "PRESENTATION_11", presentation_11;
  "AUTEURS_11", auteurs_11;
  "TITRE_TABLE_RONDE", titre_table_ronde;

  "CONTACT_ADMINISTRATIF", contact_administratif;
  "MAIL_CONTACT_ADMINISTRATIF", mail_contact_administratif;
  "TELEPHONE_ADMINISTRATIF", telephone_administratif;
  "OU_CONF", ou_conf;
  "LIEU_CONF", lieu_conf;
  "DISTANCE_DE_BORDEAUX", distance_de_bordeaux;
  "DISTANCE_DE_LYON", distance_de_lyon;
  "DISTANCE_DE_MARSEILLE", distance_de_marseille;
  "DISTANCE_DE_NANTES", distance_de_nantes;
  "DISTANCE_DE_PARIS", distance_de_paris;
  "APPROCHE_CONF", approche_conf;
  "GARE_CONF", gare_conf;
  "AEROPORT_CONF", aeroport_conf;
  "DISTANCE_AEROPORT_CONF", distance_aeroport_conf;
  "AUTRE_AEROPORT_CONF", autre_aeroport_conf;
  "GARE_PARIS", gare_paris;
  "AEROPORT_PARIS", aeroport_paris;
  "HORAIRES_VOL_ALLER_PARIS", horaires_vol_aller_paris;
  "HORAIRES_VOL_RETOUR_PARIS", horaires_vol_retour_paris;
  "HORAIRES_TRAIN_ALLER_PARIS", horaires_train_aller_paris;
  "HORAIRES_TRAIN_RETOUR_PARIS", horaires_train_retour_paris;
  "HEURE_NAVETTE_ARRIVEE", heure_navette_arrivee;
  "HEURE_NAVETTE_DEPART", heure_navette_depart;
  "HOTEL_CONF", hotel_conf;
  "ADRESSE_HOTEL_CONF", adresse_hotel_conf;
  "WEB_HOTEL_CONF", web_hotel_conf;
  "PUB_SITE_CONF", pub_site_conf;

  "MONTANT_CHAMBRE_SIMPLE", montant_chambre_simple;
  "MONTANT_CHAMBRE_DOUBLE", montant_chambre_double;
  "MONTANT_DEVISE", montant_devise;

  "INFO_TOURISTIQUES", info_touristiques;
  "SOCIAL_EVENT_CONF", social_event_conf;
  "DATE_SOCIAL_EVENT_CONF", date_social_event_conf;

  "HTTP_ROOT", http_root;
  "HTTP_CONF_SITE_ROOT", http_conf_site_root;
  "cgi",
    ("http://cristal.inria.fr/bin/inscription_jfla" ^ year_conf_str);
  "DataDirectory",
    ("/home/pauillac/cristal1/weis/Strass/CIME/jfla" ^
     year_conf_str ^ "/Data/");
  "call_back", conf_call_back
 ];;

let year_conf = year_conf_str;;
