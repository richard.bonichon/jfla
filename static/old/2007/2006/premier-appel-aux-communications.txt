(This message is intentionally written in French)

* MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER *

PREMIER APPEL AUX COMMUNICATIONS       PREMIER APPEL AUX COMMUNICATIONS

                        JFLA'2006 (http://jfla.inria.fr/)

               Journ�es Francophones des Langages Applicatifs
                        Organis�es par l'INRIA

                        28-31 janvier 2006

 JFLA'2006 est la dix-septi�me conf�rence francophone organis�e autour des
langages applicatifs et des techniques de certification bas�es sur la
d�monstration.

 Ces nouvelles journ�es se tiendront les

            28-31 janvier 2006.

 Elles auront lieu � la mer, tr�s probablement �

            Pauillac, � proximit� de Bordeaux.

 Toujours centr�e sur l'approche fonctionnelle de la programmation, la
conf�rence souhaite cette ann�e �largir son spectre aux techniques et
outils compl�mentaires qui �l�vent niveau de qualit� des logiciels
(syst�mes d'aide � la preuve, r��criture, tests, d�monstration
automatique, v�rification).

 Les JFLA r�unissent concepteurs et utilisateurs dans un
cadre agr�able facilitant la communication; ces journ�es ont pour
ambition de couvrir le domaine des langages applicatifs, en y incluant
les apports d'outils d'autres domaines qui autorisent la construction
de syst�mes logiciels plus s�rs. L'enseignement de l'approche
fonctionnelle du d�veloppement logiciel (sp�cification, s�mantiques,
programmation, compilation, certification) est �galement un sujet
concernant fortement les JFLA.

C'est pourquoi des contributions sur les th�mes suivants sont
particuli�rement recherch�es (liste non exclusive) :

- Langages fonctionnels : s�mantique, compilation, optimisation,
  mesures, tests, extensions par d'autres paradigmes de programmation.

- Sp�cification, prototypage, d�veloppements formels d'algorithmes.

- Utilisation industrielle des langages fonctionnels.

- Assistants de preuve : impl�mentation, nouvelles tactiques,
  d�veloppements pr�sentant un int�ret technique ou m�thodologique.

- Enseignement dans ses aspects li�s � l'approche fonctionnelle
  du d�veloppement.

Orateurs invit�s
----------------
 Paul Caspi (VERIMAG).
 Choukri Ben-Yell�s (IUT Valence).

Comit� de programme
-------------------
        Th�r�se Hardin, Pr�sident (LIP6, Universit� Pierre et Marie Curie, Paris)

        Pierre-Etienne Moreau, Vice-Pr�sident (projet PROTHEO, LORIA)

        Pierre Casteran (LABRI)

        Christine Paulin (LRI, Universit� d'Orsay)

        Renaud Rioboo (LIP6, UPMC)

        Xavier Urbain (CEDRIC, Institut d'Informatique d'Entreprise/CNAM)

        Alan Schmitt (projet SARDES, INRIA Grenoble)

        Bernard Serpette (projet OASIS, INRIA Sophia-Antipolis)

        Fran�ois Pessaux (Soci�t� SURLOG)

        Pierre-Yves Schobbens (Institut d'Informatique, Universit� de Namur)

Soumission
----------
Date limite de soumission : 10 octobre 2005

Les soumissions doivent �tre soit r�dig�es en fran�ais, soit
pr�sent�es en fran�ais. Elles sont limit�es � 15 pages A4. Le style
latex est impos� et se trouve sur le site WEB des journ�es � l'adresse
suivante :

         http://jfla.inria.fr/2006/actes.sty

La soumission est uniquement �lectronique, selon la m�thode d�taill�e dans

         http://jfla.inria.fr/2006/instructions-fra.html

Les soumissions sont � envoyer � la pr�sidente du comit� de programme,
avec pour titre de votre message ``SOUMISSION JFLA 2006'', � l'adresse
suivante :

             jfla2006@loria.fr

Les intentions de soumission envoy�es le plus t�t possible � l'adresse
ci-dessus seront les bienvenues.


Dates importantes
-----------------
10 octobre 2005 : Date limite de soumission
15 novembre 2005 : Notification aux auteurs
10 d�cembre 2005 : Remise des articles d�finitifs
15 janvier 2006 : Date limite d'inscription aux journ�es
28-31 janvier 2006 : Journ�es

Pour tout renseignement, contacter
----------------------------------
Marie-Fran�oise Loubressac
INRIA Rocquencourt
Bureau des Cours et Colloques (JFLA2003)
Domaine de Voluceau - BP 105
78153 Le Chesnay Cedex
T�l.: +33 (0) 1 39 63 56 00 - Fax : +33 (0) 1 39 63 56 38
email : Marie-Francoise.Loubressac@inria.fr

http://jfla.inria.fr/2006/
