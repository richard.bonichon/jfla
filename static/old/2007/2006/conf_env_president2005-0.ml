(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2004 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

(* The year when we are compiling this program! *)
let this_year = 2005;;
(* The year when the conference will hold. *)
let year_conf = 2006;;

let nieme_conf = "dix-septi�me";;

(* Dead-lines. *)
let date_limite_soumission = "10 octobre";;
let date_notification = "15 novembre";;
let date_remise_article = "10 d�cembre";;
let date_limite_inscription = "15 janvier";;

(* Dates of the meeting. *)
let date_conf = "28-31 janvier";;
let premier_jour_conf = "30 janvier";;
let deuxieme_jour_conf = "31 janvier";;
let date_arrivee_conf = "29 janvier";;

let date_arrivee_cours = "28 janvier";;
let premier_jour_cours = "28 janvier";;
let deuxieme_jour_cours = "29 janvier";;
let titre_premier_cours = "Non communiqu�";;
let titre_deuxieme_cours = "Non communiqu�";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "Th�r�se Hardin";;
let mail_president_comite = "Therese.Hardin@lip6.fr";;
let institution_president_comite =
 "LIP6, Universit� Pierre et Marie Curie, Paris";;
let mail_soumission = "jfla2006@loria.fr";;

(* Les membres du comit� de programme.
   Le membre num�ro 1 est le vice-pr�sident. *)

let membre_comite1 = "Pierre-Etienne Moreau";;
let mail_membre_comite1 = "Pierre-Etienne.Moreau@loria.fr";;
let institution_membre_comite1 = "projet PROTHEO, LORIA";;

let membre_comite2 = "Pierre Casteran";;
let mail_membre_comite2 = "Pierre.Casteran@labri.fr";;
let institution_membre_comite2 = "LABRI";;

let membre_comite3 = "Christine Paulin";;
let mail_membre_comite3 = "Christine.Paulin@lri.fr";;
let institution_membre_comite3 = "LRI, Universit� d'Orsay";;

let membre_comite4 = "Renaud Rioboo";;
let mail_membre_comite4 = "Renaud.Rioboo@lip6.fr";;
let institution_membre_comite4 = "LIP6, UPMC";;

let membre_comite5 = "Xavier Urbain";;
let mail_membre_comite5 = "Xavier.Urbain@iie.cnam.fr";;
let institution_membre_comite5 =
  "CEDRIC, Institut d'Informatique d'Entreprise/CNAM";;


let membre_comite6 = "Alan Schmitt";;
let mail_membre_comite6 = "Alan.Schmitt@inriaalpes.fr";;
let institution_membre_comite6 = "projet SARDES, INRIA Grenoble";;

let membre_comite7 = "Bernard Serpette";;
let mail_membre_comite7 = "Bernard.Serpette@inria.fr";;
let institution_membre_comite7 = "projet OASIS, INRIA Sophia-Antipolis";;

(*** la c�dille de Fran�ois ***)

let membre_comite8 = "Fran�ois Pessaux";;
let mail_membre_comite8 = "Francois_Pessaux@yahoo.fr";;
let institution_membre_comite8 = "Soci�t� SURLOG";;

let membre_comite9 = "Pierre-Yves Schobbens";;
let mail_membre_comite9 = "pys@info.fundp.ac.be";;
let institution_membre_comite9 = "Institut d'Informatique, Universit� de Namur";;

let membre_comite10 = "Sandrine Blazy-Darmon";;
let mail_membre_comite10 = "blazy@iie.cnam.fr";;
let institution_membre_comite10 = "CEDRIC, Projet CRISTAL-INRIA Rocquencourt";;

(* Les conf�renciers invit�s. *)
let orateur_invite_1 = "Paul Caspi";;
let institution_orateur_invite_1 = "VERIMAG";;
let titre_conf_invite_1 = "(Non communiqu�)";;

let orateur_invite_2 = "Choukri Ben-Yell�s";;
let institution_orateur_invite_2 = "IUT Valence";;
let titre_conf_invite_2 = "(Non communiqu�)";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
let ou_conf = "� la mer";;
let lieu_conf = "Pauillac"
let grande_ville_proche_conf = "Bordeaux";;
let gare_conf = "(Non communiqu�)";;
let aeroport_conf = "(Non communiqu�)";;
let distance_aeroport_conf = "(Non communiqu�)";;
let autre_aeroport_conf = "";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "(Non communiqu�)";;
let horaires_train_aller_paris = "(Non communiqu�)";;
let horaires_train_retour_paris = "(Non communiqu�)";;
let aeroport_paris = "(Non communiqu�)";;
let distance_de_bordeaux = "50 km";;
let distance_de_lyon = "(Non communiqu�)";;
let distance_de_marseille = "(Non communiqu�)";;
let distance_de_nantes = "(Non communiqu�)";;
let distance_de_paris = "650 km";;
let distance_de_poitiers = "(Non communiqu�)";;
let approche_conf = "(Non Communiqu�)";;

(* Navette et h�tel. *)
let gare_navette = "Bordeaux";;
let heure_navette_arrivee = "(Non communiqu�)";;
let heure_navette_depart = "(Non communiqu�)";;
let hotel_conf = "H�tel de France et d'Angleterre";;
let web_hotel_conf =
  "http://www.Hoteldefrance-Angleterre.com";;
let adresse_hotel_conf =
  Printf.sprintf "\
<BLOCKQUOTE>
%s<BR>
3 quai Albert Pichon<BR>
33250 Pauillac<BR>
Tel: 05 56 59 01 20<BR>
Fax: 05 56 59 02 31<BR>
%s
</BLOCKQUOTE>
" hotel_conf web_hotel_conf;;

let pub_site_conf =
  "<IMG SRC=\"pub_site_conf.jpg\" ALT=\"Site de la manifestation\">";;

let montant_chambre_simple = "447";;
let montant_chambre_double = "400";;
let montant_devise = "euros";;

let social_event_conf = "Visite de Cave";;
let date_social_event_conf = "Dimanche " ^ premier_jour_conf ^ " apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence. *)
let presentation_1 =
  "";;
let auteurs_1 =
  "";;
                                                                               
let presentation_2 =
  "";;
let auteurs_2 =
  "";;
 
let presentation_3 =
  "";;
let auteurs_3 =
  "";;
 
let presentation_4 =
  "";;
let auteurs_4 =
  "";;

let presentation_5 =
  "";;
let auteurs_5 =
  "";;
 
let presentation_6 =
  "";;
let auteurs_6 =
  "";;
 
let presentation_7 =
  "";;
let auteurs_7 =
  "";;
 
let presentation_8 =
  "";;
let auteurs_8 =
  "";;
 
let presentation_9 =
  "";;
let auteurs_9 =
  "";;
 
let presentation_10 =
  "";;
let auteurs_10 =
  "";;
 
let presentation_11 =
  "";;
let auteurs_11 =
  "";;

let presentation_12 =
  "";;
let auteurs_12 =
  "";;

let titre_table_ronde =
  "La place et la pratique du test \
   dans la programmation fonctionnelle - A confirmer";;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)
