(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2004 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* $Id: conf_env.ml,v 1.14 2011-09-12 22:54:07 weis Exp $ *)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

open Conf_env_president;;
(* open Conf_env_president_en;; *)

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)

(* Fields to fill to generate the Web site. *)
let nom_conf = "jfla";;
let upper_nom_conf = "JFLA";;
let long_nom_conf = "Journ�es Francophones des Langages Applicatifs";;

(* Fin des donn�es d�pendantes de la conf�rence. *)

let montant_chambre_simple = Conf_env_president.montant_chambre_simple;;
let montant_chambre_double = Conf_env_president.montant_chambre_double;;

let date_depart_conf = deuxieme_jour_conf;;

let mail_reception_inscriptions = Conf_env_president.mail_reception_inscriptions;;

(* Emplacement des sites Webs. *)

let url_concat s1 s2 = Printf.sprintf "%s/%s" s1 s2;;

let conf_http_root = Printf.sprintf "http://%s.inria.fr" nom_conf;;
let conf_http_site_root = conf_http_root;;

let conf_cgi_bin_root_url = url_concat "/cgi-bin" nom_conf;;
let conf_data_root_dir = "/home/yquem/guest/jfla";;

let info_touristiques =
  "\
   <UL>\
 \n   <LI><A HREF=\""
  ^ web_hotel_conf ^
  "\"></A>.\
 \n</UL>\
  "
;;

let adresse_hotel_conf =
  Printf.sprintf "\
    <BLOCKQUOTE>\
  \n  %s<BR>\
  \n  %s\
  \n</BLOCKQUOTE>\
  \n" Conf_env_president.hotel_conf Conf_env_president.web_hotel_conf
;;

let conf_call_back = "treat_form";;

let last_year_conf = year_conf - 1;;
let next_year_conf = year_conf + 1;;

let year_conf_str = Printf.sprintf "%d" year_conf;;
let year_conf_short_str = Printf.sprintf "%#02d" (year_conf - 2000);;
let year_compile_str = Printf.sprintf "%d" year_compile;;
let year_soumission_str = Printf.sprintf "%d" year_soumission;;

let env = [
  "NOM_CONF", nom_conf;
  "UPPER_NOM_CONF", upper_nom_conf;
  "LONG_NOM_CONF", long_nom_conf;

  "YEAR_CONF_SHORT", year_conf_short_str;
  "YEAR_CONF", year_conf_str;
  "YEAR_COMPILE", year_compile_str;
  "LAST_YEAR_CONF", string_of_int last_year_conf;
  "NEXT_YEAR_CONF", string_of_int next_year_conf;
  "NIEME_CONF", nieme_conf;
  "DATE_CONF", date_conf;
  "MONTH_CONF", month_conf;

  "DATE_ARRIVEE_CONF", date_arrivee_conf;
  "DATE_DEPART_CONF", date_depart_conf;
  "PREMIER_JOUR_CONF", premier_jour_conf;
  "DEUXIEME_JOUR_CONF", deuxieme_jour_conf;

  "DATE_ARRIVEE_COURS", date_arrivee_cours;

  "JOUR_COURS_1", jour_cours_1;
  "HEURE_COURS_1", heure_cours_1;
  "TITRE_COURS_1", titre_cours_1;
  "AUTEUR_COURS_1", auteur_cours_1;
  "MAIL_AUTEUR_COURS_1", mail_auteur_cours_1;
  "INSTITUTION_AUTEUR_COURS_1", institution_auteur_cours_1;
  "HEURE_COURS_1", heure_cours_1;

  "JOUR_COURS_2", jour_cours_2;
  "HEURE_COURS_2", heure_cours_2;
  "TITRE_COURS_2", titre_cours_2;
  "AUTEUR_COURS_2", auteur_cours_2;
  "MAIL_AUTEUR_COURS_2", mail_auteur_cours_2;
  "INSTITUTION_AUTEUR_COURS_2", institution_auteur_cours_2;
  "HEURE_COURS_2", heure_cours_2;

  "PHRASE_ACTES", phrase_actes;
  "YEAR_SOUMISSION", year_soumission_str;
  "DATE_LIMITE_SOUMISSION", date_limite_soumission;
  "DATE_NOTIFICATION", date_notification;
  "DATE_REMISE_ARTICLE", date_remise_article;
  "DATE_LIMITE_INSCRIPTION", date_limite_inscription;

  "MAIL_SOUMISSION", mail_soumission;
  "PHRASE_SOUMISSION", phrase_soumission;

  "PRESIDENT_COMITE", president_comite;
  "PRESIDENT_CIVILITE", Conf_env_president.president_civilite;
  "MAIL_PRESIDENT_COMITE", mail_president_comite;
  "INSTITUTION_PRESIDENT_COMITE", institution_president_comite;

  "VICE_PRESIDENT_COMITE", vice_president_comite;
  "VICE_PRESIDENT_CIVILITE", Conf_env_president.vice_president_civilite;
  "MAIL_VICE_PRESIDENT_COMITE", mail_vice_president_comite;
  "INSTITUTION_VICE_PRESIDENT_COMITE", institution_vice_president_comite;

  "MEMBRE_COMITE_2", membre_comite_2;
  "MAIL_MEMBRE_COMITE_2", mail_membre_comite_2;
  "INSTITUTION_MEMBRE_COMITE_2", institution_membre_comite_2;

  "MEMBRE_COMITE_3", membre_comite_3;
  "MAIL_MEMBRE_COMITE_3", mail_membre_comite_3;
  "INSTITUTION_MEMBRE_COMITE_3", institution_membre_comite_3;

  "MEMBRE_COMITE_4", membre_comite_4;
  "MAIL_MEMBRE_COMITE_4", mail_membre_comite_4;
  "INSTITUTION_MEMBRE_COMITE_4", institution_membre_comite_4;

  "MEMBRE_COMITE_5", membre_comite_5;
  "MAIL_MEMBRE_COMITE_5", mail_membre_comite_5;
  "INSTITUTION_MEMBRE_COMITE_5", institution_membre_comite_5;

  "MEMBRE_COMITE_6", membre_comite_6;
  "MAIL_MEMBRE_COMITE_6", mail_membre_comite_6;
  "INSTITUTION_MEMBRE_COMITE_6", institution_membre_comite_6;

  "MEMBRE_COMITE_7", membre_comite_7;
  "MAIL_MEMBRE_COMITE_7", mail_membre_comite_7;
  "INSTITUTION_MEMBRE_COMITE_7", institution_membre_comite_7;

  "MEMBRE_COMITE_8", membre_comite_8;
  "MAIL_MEMBRE_COMITE_8", mail_membre_comite_8;
  "INSTITUTION_MEMBRE_COMITE_8", institution_membre_comite_8;

  "MEMBRE_COMITE_9", membre_comite_9;
  "MAIL_MEMBRE_COMITE_9", mail_membre_comite_9;
  "INSTITUTION_MEMBRE_COMITE_9", institution_membre_comite_9;

(*
  "MEMBRE_COMITE_10", membre_comite_10;
  "MAIL_MEMBRE_COMITE_10", mail_membre_comite_10;
  "INSTITUTION_MEMBRE_COMITE_10", institution_membre_comite_10;
*)

  "ORATEUR_INVITE_1", orateur_invite_1;
  "INSTITUTION_ORATEUR_INVITE_1", institution_orateur_invite_1;
  "TITRE_CONF_INVITE_1", titre_conf_invite_1;
  "ORATEUR_INVITE_2", orateur_invite_2;
  "INSTITUTION_ORATEUR_INVITE_2", institution_orateur_invite_2;
  "TITRE_CONF_INVITE_2", titre_conf_invite_2;

  "PRESENTATION_1", presentation_1;
  "AUTEURS_1", auteurs_1;
  "PRESENTATION_2", presentation_2;
  "AUTEURS_2", auteurs_2;
  "PRESENTATION_3", presentation_3;
  "AUTEURS_3", auteurs_3;
  "PRESENTATION_4", presentation_4;
  "AUTEURS_4", auteurs_4;
  "PRESENTATION_5", presentation_5;
  "AUTEURS_5", auteurs_5;
  "PRESENTATION_6", presentation_6;
  "AUTEURS_6", auteurs_6;
  "PRESENTATION_7", presentation_7;
  "AUTEURS_7", auteurs_7;
  "PRESENTATION_8", presentation_8;
  "AUTEURS_8", auteurs_8;
  "PRESENTATION_9", presentation_9;
  "AUTEURS_9", auteurs_9;
  "PRESENTATION_10", presentation_10;
  "AUTEURS_10", auteurs_10;
  "PRESENTATION_11", presentation_11;
  "AUTEURS_11", auteurs_11;
(*  "PRESENTATION_12", presentation_12;
  "AUTEURS_12", auteurs_12;
  "TITRE_TABLE_RONDE", titre_table_ronde;
*)

  "CONTACT_ADMINISTRATIF", contact_administratif;
  "MAIL_CONTACT_ADMINISTRATIF", mail_contact_administratif;
  "ADRESSE_CONTACT_ADMINISTRATIF", adresse_contact_administratif;
  "TELEPHONE_CONTACT_ADMINISTRATIF", telephone_contact_administratif;

  "PAGE_INSCRIPTIONS", page_inscriptions;

  "OU_CONF", ou_conf;
  "LIEU_CONF", lieu_conf;
  "GRANDE_VILLE_PROCHE_CONF", grande_ville_proche_conf;
  "DISTANCE_DE_BORDEAUX", distance_de_bordeaux;
  "DISTANCE_DE_LYON", distance_de_lyon;
  "DISTANCE_DE_MARSEILLE", distance_de_marseille;
  "DISTANCE_DE_NANTES", distance_de_nantes;
  "DISTANCE_DE_PARIS", distance_de_paris;
  "APPROCHE_CONF", approche_conf;
  "GARE_CONF", gare_conf;
  "AEROPORT_CONF", aeroport_conf;
  "DISTANCE_AEROPORT_CONF", distance_aeroport_conf;
  "AUTRE_AEROPORT_CONF", autre_aeroport_conf;
  "GARE_PARIS", gare_paris;
  "AEROPORT_PARIS", aeroport_paris;
  "HORAIRES_VOL_ALLER_PARIS", horaires_vol_aller_paris;
  "HORAIRES_VOL_RETOUR_PARIS", horaires_vol_retour_paris;
  "HORAIRES_TRAIN_ALLER_PARIS", horaires_train_aller_paris;
  "HORAIRES_TRAIN_RETOUR_PARIS", horaires_train_retour_paris;
  "GARE_NAVETTE", gare_navette;
  "HEURE_NAVETTE_ARRIVEE", heure_navette_arrivee;
  "HEURE_NAVETTE_DEPART", heure_navette_depart;

  "HOTEL_CONF", hotel_conf;
  "ADRESSE_HOTEL_CONF", adresse_hotel_conf;
  "WEB_HOTEL_CONF", web_hotel_conf;
  "PHOTO_HOTEL_CONF", photo_hotel_conf;
  "PHOTO_SITE_CONF", photo_site_conf;

  "MONTANT_CHAMBRE_SIMPLE", montant_chambre_simple;
  "MONTANT_CHAMBRE_DOUBLE", montant_chambre_double;
  "MONTANT_DEVISE", montant_devise;

  "INFO_TOURISTIQUES", info_touristiques;
  "SOCIAL_EVENT_CONF", social_event_conf;
  "DATE_SOCIAL_EVENT_CONF", date_social_event_conf;

  "HTTP_ROOT", conf_http_root;
  "HTTP_CONF_SITE_ROOT", conf_http_site_root;
  "cgi",
    (url_concat conf_cgi_bin_root_url
      (Printf.sprintf "inscription_%s%s" nom_conf year_conf_str));
  "DataDirectory",
    (Filename.concat
       conf_data_root_dir
       (Filename.concat year_conf_str "Data"));
  "call_back", conf_call_back;

(* for english *)
(*
"NIEME_CONF_EN", nieme_conf_en;
"DATE_CONF_EN", date_conf_en;
"MONTH_CONF_EN", month_conf_en;

"DATE_ARRIVEE_CONF_EN", date_arrivee_conf_en;
"PREMIER_JOUR_CONF_EN", premier_jour_conf_en;
"DEUXIEME_JOUR_CONF_EN", deuxieme_jour_conf_en;

"DATE_ARRIVEE_COURS_EN", date_arrivee_cours_en;

"PHRASE_ACTES_EN", phrase_actes_en;
"DATE_LIMITE_SOUMISSION_EN", date_limite_soumission_en;
"DATE_NOTIFICATION_EN", date_notification_en;
"DATE_REMISE_ARTICLE_EN", date_remise_article_en;
"DATE_LIMITE_INSCRIPTION_EN", date_limite_inscription_en;

"PRESIDENT_CIVILITE_EN", Conf_env_president_en.president_civilite_en;
"VICE_PRESIDENT_CIVILITE_EN", Conf_env_president_en.vice_president_civilite_en;
"OU_CONF_EN", ou_conf_en;
"HORAIRES_VOL_ALLER_PARIS_EN", horaires_vol_aller_paris_en;
"HORAIRES_VOL_RETOUR_PARIS_EN", horaires_vol_retour_paris_en;
"HORAIRES_TRAIN_ALLER_PARIS_EN", horaires_train_aller_paris_en;
"HORAIRES_TRAIN_RETOUR_PARIS_EN", horaires_train_retour_paris_en;
"HEURE_NAVETTE_ARRIVEE_EN", heure_navette_arrivee_en;
"HEURE_NAVETTE_DEPART_EN", heure_navette_depart_en;

"SOCIAL_EVENT_CONF_EN", social_event_conf_en;
"DATE_SOCIAL_EVENT_CONF_EN", date_social_event_conf_en;
*)			
(* end english *)
]
;;

let year_conf = year_conf_str;;
