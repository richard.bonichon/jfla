� giavitto@lri.fr, michel@lami.univ-evry.fr

Bonjour,

F�licitations: votre article soumis aux JFLA'99 a �t� accept�, apr�s
relecture du comit� de programme.

Je vous rappelle que le style latex est impos� pour la version finale
de votre papier. Vous trouverez ce style � l'adresse:

http://pauillac.inria.fr/~weis/jfla99/jfla99.sty

La date de remise des articles d�finitifs est le 7 d�cembre 1998.

Je vous prie de m'envoyer votre article en format PostScript, par un
message �lectronique ayant pour sujet

Article JFLA99

Merci d'avance, et encore bravo pour votre travail.

� bient�t aux JFLA

Pierre Weis (Pierre.Weis@inria.fr)

-----------------------------------------------------
<P>Nom du papier et auteurs: Calcul distribu� de champs de donn�es
<P>    Jean-Louis Giavitto, Olivier Michel.

<H2>1</H2>

<P>Notation du papier: (de 1 � 5) 4
<P> (1: � rejeter; 2: inint�ressant; 3: acceptable;
<P>  4: bon papier; 5: tr�s bon papier)

<P>Commentaires aux auteurs:
<P>    Il s'agit d'un bon article non technique pr�sentant
tant les s�mantiques que la r�alisation pratique
d'un syst�me distribu� r�solvant des �quations
8 1/2.  J'insiste sur l'aspect non technique : on reste
un peu sur sa faim quant aux d�tails, tant s�mantiques
que pratiques.  Les s�mantiques $[[\_]]_i$, $0\leq i\leq 2$,
ne sont pas d�crites, par exemple, et le lecteur est
constamment pri� d'imaginer ce qu'il mettrait \`a la
place des d�finitions manquantes des auteurs.  Alors
que les descriptions de ${\cal L}_0$ et ${\cal L}_1$
permettent encore de se faire une id�e, celle de ${\cal L}_2$
est presque totalement �lid�e.  Le paragraphe allant du bas
de la page 13 au haut de la page 14 est en particulier extr\^emement
allusif (d�finition de $\cal E$ ?).  De plus, il est toujours
possible qu'une expression de ${\cal L}_1$, m\^eme r�duite,
reste en grande partie intensionnelle ($map [+] 1 2$, par ex.).
Comment fait-on pour la r�duire ?  Ou bien une erreur est-elle
signal�e ?  Ce genre de questions n'a pas de r�ponse dans
l'article.

-----------------------------------------------------

<H2>2</H2>

<P>Notation du papier: (de 1 � 5) 2
<P> (1: � rejeter; 2: inint�ressant; 3: acceptable;
<P>  4: bon papier; 5: tr�s bon papier)

<P>Commentaires aux auteurs:

<P>Ce papier pr�sente une apparence de s�rieux, mais je m'avoue
totalement incapable de juger la qualit� du travail, en raison de
nombreuses lacunes dans l'expos�. Tout le discours est organis� autour
des pens�es propres aux auteurs, sans un effort didactique suffisant
pour pr�senter le syst�me aux novices, et pour rendre le papier
accessible � quelqu'un qui n'aurait pas lu les travaux ant�rieurs de
cette �quipe (r�fs 4, 5, 6, 11, 12).  Par ex. les acronymes GBF, HPF
ne sont pas n�cessairement connus de tout un chacun. En l'absence
d'une syntaxe et s�mantique compl�te, j'ai des doutes et inqui�tudes
sur la coh�rence globale des constructions linguistiques propos�es.

<P>Ex:
<P>- quelle est la signature de l'op�rateur "map". Il semble utilis�
parfois avec un op�rateur binaire comme argument, parfois avec un
op�rateur unaire. S'agit-il de surcharge? de curryfication? 

<P>- "merge": le nom est particuli�rement mal choisi, puisqu'il laisse
croire � une op�ration sym�trique alors qu'en l'occurence il s'agit
d'une "union asymetrique".

<P>- Preuves? les propri�t�es �nonc�es dans la Fig. 3 (commutation du
diagramme) et en bas de page 11 sont-elles prouv�es? dans des travaux
ant�rieurs? si oui, lesquels?


<P>Par ailleurs, la distinction entre les travaux ant�rieurs et la
contribution propre de cet article n'est pas clairement �tablie. Les
langages L1, L2, L3 faisaient-ils d�j� partie de "81/2"? Si non,
sommes-nous assur�s que ces 3 langages respectent la s�mantique de
d�part de "81/2"?

<P>En r�sum�, le papier contient probablement des �l�ments de qualit�,
mais en l'�tat me para�t impropre � la publication; j'encourage les
auteurs � revoir leur copie.

<P>Pour finir, quelque typos:
<P>p2: r�sultats sont encourageantS
<P>p3, fig 1: associ� � chaque sommet S (pas sommets)
<P>p10: ne prend pas une forme (pas prends)
<P>p12: pr�dicats �l�mentaires correspondant � une (pas correspondants)
<P>p14: d'autres choix auraiENt �t� possibles

