(This message is intentionally written in French)

* MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER *

DEUXIEME APPEL AUX COMMUNICATIONS       DEUXIEME APPEL AUX COMMUNICATIONS

                        JFLA'2009 (http://jfla.inria.fr/)

               Journ�es Francophones des Langages Applicatifs
                        Organis�es par l'INRIA

                        31 janvier au 3 f�vrier 2009

JFLA'2009 est la vingti�me conf�rence francophone organis�e autour des
langages applicatifs et des techniques de certification bas�es sur la
d�monstration.

Ces nouvelles journ�es se tiendront les

            31 janvier au 3 f�vrier 2009.

Elles auront lieu � la montagne, �

            Saint-Quentin sur Is�re, au pied du Vercors, � proximit� de Grenoble.

Toujours centr�e sur l'approche fonctionnelle de la programmation, la
conf�rence porte �galement sur les techniques et outils compl�mentaires qui
�l�vent le niveau de qualit� des logiciels (syst�mes d'aide � la preuve,
r��criture, tests, d�monstration automatique, v�rification).

Les JFLA r�unissent concepteurs et utilisateurs dans un cadre
agr�able qui facilite la communication; ces journ�es ont pour ambition de
couvrir le domaine des langages applicatifs au sens large, en y incluant les
apports d'outils d'autres domaines qui permettent la construction de syst�mes
logiciels plus s�rs. L'enseignement de l'approche fonctionnelle du
d�veloppement logiciel (sp�cification, s�mantiques, programmation, compilation,
certification) est �galement un sujet qui concerne au plus haut point les
JFLA.

C'est pourquoi des contributions sur les th�mes suivants sont
particuli�rement recherch�es (liste non exclusive) :

- Langages fonctionnels et applicatifs : s�mantique, compilation, optimisation,
  mesures, tests, extensions par d'autres paradigmes de programmation.

- Sp�cification, prototypage, d�veloppements formels d'algorithmes.

- Utilisation industrielle des langages fonctionnels et applicatifs.

- Assistants de preuve : impl�mentation, nouvelles tactiques,
  d�veloppements pr�sentant un int�r�t technique ou m�thodologique.

- Enseignement dans ses aspects li�s � l'approche fonctionnelle
  du d�veloppement.

Les JFLA cherchent avant tout des articles de recherche originaux qui apportent
une r�elle nouveaut�. Toutefois, un article traitant d'un sujet qui int�resse
plusieurs disciplines sera examin� avec soin, m�me s'il a pr�alablement �t�
pr�sent� � une autre communaut� sans rapport avec celle des JFLA.
Un article ayant �t� traduit en fran�ais � partir d'une publication r�cente en
anglais sera examin�, � condition que la traduction apporte un �l�ment
nouveau.

Les articles soumis aux JFLA sont relus par au moins 2 personnes s'ils sont
accept�s, 3 personnes s'ils sont rejet�s.

Les critiques des relecteurs sont toujours bienveillantes et la plupart du
temps encourageantes et constructives, m�me en cas de rejet.

Il n'y a donc pas de raison de ne pas soumettre aux JFLA !

Orateurs invit�s
----------------
 Vincent Balat (Universit� Paris 7): � Ocsigen : approche fonctionnelle typ�e de la programmation Web �
 Bruno Barras (Trusted Labs): � Faut-il avoir peur de sa carte SIM ? �


Cours
-----
 G�rard Huet (INRIA Paris-Rocquecourt): � Automates, transducteurs et machines d'Eilenberg applicatives dans la bo�te � outils Zen. Applications au traitement de la langue. �
 Assia Mahboubi (LIX, INRIA Saclay - �le-de-France): � Pr�sentation de SSReflect �

Comit� de programme
-------------------
        Alan Schmitt, pr�sident (LIG, INRIA Grenoble - Rh�ne-Alpes)

        Micaela Mayero, pr�sidente (LIPN, Universit� Paris 13)

        Boutheina Chetali (Gemalto)

        Sylvain Conchon (LRI, Universit� Paris-Sud)

        David Delahaye (CEDRIC, CNAM)

        Hugo Herbelin (LIX, INRIA Saclay - �le-de-France)

        Didier Le Botlan (LAAS-CNRS, INSA de Toulouse)

        Jean-Vincent Loddo (LIPN, Universit� Paris 13)

        Alexandre Miquel (PPS, Universit� Paris 7)

        Davide Sangiorgi (Universit� de Bologne)

Soumission
----------
Date limite de soumission : 22 octobre 2008

Les soumissions doivent �tre soit r�dig�es en fran�ais, soit
pr�sent�es en fran�ais. Elles sont limit�es � 15 pages A4. Le style
latex est impos� et se trouve sur le site WEB des journ�es � l'adresse
suivante :

         http://jfla.inria.fr/2009/actes.sty

La soumission est uniquement �lectronique, selon la m�thode d�taill�e dans

         http://jfla.inria.fr/2009/instructions.fr.html

Les soumissions sont � envoyer au pr�sident du comit� de programme,
avec pour titre de votre message ``SOUMISSION JFLA 2009'', � l'adresse
suivante :

             alan.schmitt@inria.fr

Les intentions de soumission envoy�es le plus t�t possible � l'adresse
ci-dessus seront les bienvenues.


Dates importantes
-----------------
22 octobre 2008 : Date limite de soumission
21 novembre 2008 : Notification aux auteurs
10 d�cembre 2008 : Remise des articles d�finitifs
14 janvier 2009 : Date limite d'inscription aux journ�es
31 janvier au 3 f�vrier 2009 : Journ�es

Pour tout renseignement, contacter
----------------------------------

INRIA Grenoble Rh�ne-Alpes
Bureau des Cours-Colloques
655 avenue de l'Europe - Montbonnot
38334 Saint Ismier Cedex - France

Tel : + 33 (0)4 76 61 52 23 - Fax : + 33 (0)4 76 61 52 06
email : colloques@inrialpes.fr

http://jfla.inria.fr/2009/
