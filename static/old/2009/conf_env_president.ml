(***********************************************************************)
(*                                                                     *)
(*                           CIME Caml                                 *)
(*                                                                     *)
(*            Pierre Weis, projet Cristal, INRIA Rocquencourt          *)
(*                                                                     *)
(* Copyright 2001, 2008 Institut National de Recherche en Informatique *)
(* et en Automatique.  Distributed only by permission.                 *)
(*                                                                     *)
(***********************************************************************)

(* To generate the form to subscribe to the Conference JFLA.
   Each variable listed in env, is replaced by the associated value
   string, during the expansion phase from .data to .html files. *)

(* The year when we are compiling this program! *)
let this_year = 2008;;
(* The year when the conference will hold. *)
let year_conf = 2009;;
(* The month when the conference will hold. *)
let month_conf = "janvier";;

let nieme_conf = "vingti�me";;

(* Dead-lines.
   All these dead-lines are supposed to be in year this_year
   (i.e. could be the year before year_conf). *)
let date_limite_soumission = "22 octobre";;
let date_notification = "21 novembre";;
let date_remise_article = "10 d�cembre";;

(* This deadline implicitely takes place in year year_conf. *)
let date_limite_inscription = "14 janvier";;

(* Dates of the meeting.
   All those dates implicitely takes place in year year_conf. *)
let date_conf = "31 janvier au 3 f�vrier";;
let premier_jour_conf = "2 f�vrier";;
let deuxieme_jour_conf = "3 f�vrier";;
let date_arrivee_conf = "31 janvier";;

(* Dates des cours.
   All those dates implicitely takes place in year year_conf. *)
let date_arrivee_cours = "31 janvier";;
let jour_cours_1 = "31 janvier";;
let heure_cours_1 = "(Non communiqu�)";;
let jour_cours_2 = "1er f�vrier";;
let heure_cours_2 = "(Non communiqu�)";;

(* Program Commitee. *)

(* Le pr�sident du comit� de programme. *)
let president_comite = "Alan Schmitt";;
let president_civilite = "pr�sident";;
let vice_president_civilite = "pr�sidente";;
let mail_president_comite = "alan.schmitt@inria.fr";;
let institution_president_comite =
 "LIG, INRIA Grenoble - Rh�ne-Alpes";;
let mail_soumission = "alan.schmitt@inria.fr";;

(* Les membres du comit� de programme.
   Le membre num�ro 1 est le vice-pr�sident. *)

let membre_comite_1 = "Micaela Mayero";;
let mail_membre_comite_1 = "Micaela.Mayero@lipn.univ-paris13.fr";;
let institution_membre_comite_1 = "LIPN, Universit� Paris 13";;

let membre_comite_2 = "Boutheina Chetali";;
let mail_membre_comite_2 = "boutheina.chetali@gemalto.com";;
let institution_membre_comite_2 = "Gemalto";;

let membre_comite_3 = "Sylvain Conchon";;
let mail_membre_comite_3 = "Sylvain.Conchon@lri.fr";;
let institution_membre_comite_3 = "LRI, Universit� Paris-Sud";;

let membre_comite_4 = "David Delahaye";;
let mail_membre_comite_4 = "David.Delahaye@cnam.fr";;
let institution_membre_comite_4 = "CEDRIC, CNAM";;

let membre_comite_5 = "Hugo Herbelin";;
let mail_membre_comite_5 = "Hugo.Herbelin@inria.fr";;
let institution_membre_comite_5 = "LIX, INRIA Saclay - �le-de-France";;

let membre_comite_6 = "Didier Le Botlan";;
let mail_membre_comite_6 = "didier.le-botlan@insa-toulouse.fr";;
let institution_membre_comite_6 = "LAAS-CNRS, INSA de Toulouse";;

let membre_comite_7 = "Jean-Vincent Loddo";;
let mail_membre_comite_7 = "loddo@lipn.univ-paris13.fr";;
let institution_membre_comite_7 = "LIPN, Universit� Paris 13";;

let membre_comite_8 = "Alexandre Miquel";;
let mail_membre_comite_8 = "Alexandre.Miquel@pps.jussieu.fr";;
let institution_membre_comite_8 = "PPS, Universit� Paris 7";;

let membre_comite_9 = "Davide Sangiorgi";;
let mail_membre_comite_9 = "Davide.Sangiorgi@cs.unibo.it";;
let institution_membre_comite_9 = "Universit� de Bologne";;

(* Les conf�renciers invit�s. *)
let orateur_invite_1 = "Vincent Balat";;
let mail_orateur_invite_1 = "vincent.balat@pps.jussieu.fr";;
let institution_orateur_invite_1 = "Universit� Paris 7";;
let titre_conf_invite_1 = "Ocsigen : approche fonctionnelle typ�e de la programmation Web";;

let orateur_invite_2 = "Bruno Barras";;
let mail_orateur_invite_2 = "bruno.barras@inria.fr";;
let institution_orateur_invite_2 = "Trusted Labs";;
let titre_conf_invite_2 = "Faut-il avoir peur de sa carte SIM ?";;

(* Les cours et leurs auteurs. *)
let auteur_cours_1 = "G�rard Huet";;
let mail_auteur_cours_1 = "gerard.huet@inria.fr";;
let institution_auteur_cours_1 = "INRIA Paris-Rocquecourt";;
let titre_cours_1 =
  "Automates, transducteurs et machines d'Eilenberg applicatives dans la bo�te � outils Zen. Applications au traitement de la langue.";;

let auteur_cours_2 = "Assia Mahboubi";;
let mail_auteur_cours_2 = "assia.mahboubi@inria.fr";;
let institution_auteur_cours_2 = "LIX, INRIA Saclay - �le-de-France";;
let titre_cours_2 =
  "Pr�sentation de SSReflect";;

(* Lieu de la conf�rence et renseignements sur le voyage. *)
let ou_conf = "� la montagne";;
let lieu_conf = "Saint-Quentin sur Is�re, au pied du Vercors";;
let grande_ville_proche_conf = "Grenoble";;
let gare_conf = "Grenoble";;
let aeroport_conf = "Grenoble";;
let distance_aeroport_conf = "(Non communiqu�)";;
let autre_aeroport_conf = "Lyon Satolas";;
let horaires_vol_aller_paris = "(Non communiqu�)";;
let horaires_vol_retour_paris = "(Non communiqu�)";;
let gare_paris = "Gare de Lyon";;
let horaires_train_aller_paris = "d�part 7h50, arriv�e 10h50";;
let horaires_train_retour_paris = "d�part 16h05, arriv�e 19h07";;
let aeroport_paris = "(Non communiqu�)";;
let distance_de_bordeaux = "(Non communiqu�)";;
let distance_de_lyon = "(Non communiqu�)";;
let distance_de_marseille = "(Non communiqu�)";;
let distance_de_nantes = "(Non communiqu�)";;
let distance_de_paris = "(Non communiqu�";;
let distance_de_poitiers = "(Non communiqu�)";;
let approche_conf = "(Non Communiqu�)";;

(* Navette et h�tel. *)
let gare_navette = "Gare de Grenoble";;
let heure_navette_arrivee = "11h30";;
let heure_navette_depart = "14h00";;
let hotel_conf = "Golf H�tel de Grenoble-Charmeil
<BR>38210 Saint-Quentin sur is�re
<BR>T�l. : 33 (0)4 76 93 67 28
<BR>Fax : 33 (0)4 76 93 62 04
<BR>Email : info@golfhotelgrenoble.com"
;;
let web_hotel_conf = "http://www.golfhotelcharmeil.com/";;
let adresse_hotel_conf =
  Printf.sprintf "\
<BLOCKQUOTE>
%s<BR>
%s
</BLOCKQUOTE>
" hotel_conf web_hotel_conf;;

(* Le contact administratif pour le pr�sident et les conf�renciers. *)
let contact_administratif = "";;
let mail_contact_administratif = "colloques@inrialpes.fr";;
let adresse_contact_administratif = "\
  INRIA Grenoble Rh�ne-Alpes\n\
  Bureau des Cours-Colloques\n\
  655 avenue de l'Europe - Montbonnot\n\
  38334 Saint Ismier Cedex - France\n"
;;
let telephone_contact_administratif =
  "Tel : + 33 (0)4 76 61 52 23 - Fax : + 33 (0)4 76 61 52 06"
;;
let mail_reception_inscriptions = "colloques@inrialpes.fr";;
let page_inscriptions = "http://registration.net-resa.com/cgi-bin/WebObjects/gnetresa.woa/wa/newParticipant?idevt=329&profil=542"
;;

(* Une image pour symboliser le lieu de la conf�rence. On la prend en g�n�ral
   sur le site Ou�be du syndicat d'initiative. *)
let pub_site_conf =
  "<IMG SRC=\"pub_site_conf.jpg\" ALT=\"Site de la manifestation\">";;
(* Une image du site de la conf�rence. En g�n�ral une photo de l'h�tel. *)
let site_conf =
  "<IMG SRC=\"site_conf.jpg\" ALT=\"Lieu de la conf�rence\">";;

let montant_chambre_simple = "(Non communiqu�)";;
let montant_chambre_double = "(Non communiqu�)";;
let montant_devise = "euros";;

let social_event_conf = "une balade � pied";;
let date_social_event_conf = "dimanche " ^ deuxieme_jour_conf ^ " apr�s-midi";;

(* Le programme de la conf�rence avec les heures de passages des
   pr�sentations de la conf�rence. *)
let presentation_1 =
  "Qui s�me la fonction, r�colte le tuyau typ�";;
let auteurs_1 = "Didier Parigot et Bernard Paul Serpette";;

let presentation_2 =
  "Foncteurs imp�ratifs et compos�s: la notion de projets dans Frama-C";;
let auteurs_2 =
  "Julien Signoles";;
 
let presentation_3 =
  "Vers une programmation fonctionnelle en appel par valeur sur syst�mes multi-coeurs : �valuation asynchrone et ramasse-miettes parall�le";;
let auteurs_3 = "Luca Saiu";;
 
let presentation_4 =
  "V�rification d'invariants pour des syst�mes sp�cifi�s en logique de r��criture";;
let auteurs_4 =
  "Vlad Rusu et Manuel Clavel ";;

let presentation_5 =
  "Un mod�le de l'assistant � la preuve: PAF!";;
let auteurs_5 =
  "S�verine Maingaud";;
 
let presentation_6 =
  "Extraction certifi�e dans Coq-en-Coq";;
let auteurs_6 =
  "St�phane Glondu";;
 
let presentation_7 =
  "Abstraction d'horloges dans les syst�mes synchrones flot de donn�es";;
let auteurs_7 =
  "Louis Mandel et Florence Plateau";;

let presentation_8 =
  "";;
let auteurs_8 =
  "";;

let presentation_9 =
  "Fouille au code OCaml par analyse de d�pendances";;
let auteurs_9 =
  "Maxence Guesdon";;
 
let presentation_10 =
  "Faire bonne figure avec Mlpost";;
let auteurs_10 =
  "R. Bardou, J. Kanig, J.-C. Filli�tre et S. Lescuyer";;
 
let presentation_11 =
  "";;
let auteurs_11 =
  "";;
 
let titre_table_ronde =
  "";;

(* Fin des donn�es d�pendantes du pr�sident du comit� de programme. *)
