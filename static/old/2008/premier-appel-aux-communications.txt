(This message is intentionally written in French)

* MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER * MERCI DE FAIRE CIRCULER *

PREMIER APPEL AUX COMMUNICATIONS       PREMIER APPEL AUX COMMUNICATIONS

                        JFLA'2008 (http://jfla.inria.fr/)

               Journ�es Francophones des Langages Applicatifs
                        Organis�es par l'INRIA

                        26 au 29 janvier 2008

 JFLA'2008 est la dix-neuvi�me conf�rence francophone organis�e autour des
langages applicatifs et des techniques de certification bas�es sur la
d�monstration.

 Ces nouvelles journ�es se tiendront les

            26 au 29 janvier 2008.

 Elles auront lieu � la mer, tr�s probablement �

            �tretat, � proximit� de �Le Havre�.

 Toujours centr�e sur l'approche fonctionnelle de la programmation, la
conf�rence �largit son spectre aux techniques et outils compl�mentaires qui
�l�vent le niveau de qualit� des logiciels (syst�mes d'aide � la preuve,
r��criture, tests, d�monstration automatique, v�rification).

 Les JFLA r�unissent concepteurs et utilisateurs dans un cadre
agr�able qui facilite la communication; ces journ�es ont pour ambition de
couvrir le domaine des langages applicatifs au sens large, en y incluant les
apports d'outils d'autres domaines qui permettent la construction de syst�mes
logiciels plus s�rs. L'enseignement de l'approche fonctionnelle du
d�veloppement logiciel (sp�cification, s�mantiques, programmation, compilation,
certification) est �galement un sujet qui concerne au plus haut point les
JFLA.

C'est pourquoi des contributions sur les th�mes suivants sont
particuli�rement recherch�es (liste non exclusive) :

- Langages fonctionnels : s�mantique, compilation, optimisation,
  mesures, tests, extensions par d'autres paradigmes de programmation.

- Sp�cification, prototypage, d�veloppements formels d'algorithmes.

- Utilisation industrielle des langages fonctionnels.

- Assistants de preuve : impl�mentation, nouvelles tactiques,
  d�veloppements pr�sentant un int�ret technique ou m�thodologique.

- Enseignement dans ses aspects li�s � l'approche fonctionnelle
  du d�veloppement.

Les JFLA s'occupent avant tout d'articles de recherche originaux qui apportent
une r�elle nouveaut�. Toutefois, un article traitant d'un sujet qui int�resse
plusieurs disciplines sera examin� avec soin, m�me s'il a pr�alablement �t�
pr�sent� � une autre communaut� sans rapport avec celle des JFLA.
Un article ayant �t� traduit en fran�ais � partir d'une publication r�cente en
anglais sera examin�, � condition que la traduction apporte de plus un �l�ment
nouveau.

Les articles soumis aux JFLA sont relus par au moins 2 personnes s'ils sont
accept�s, 3 personnes s'ils sont rejet�s.

Les critiques des relecteurs sont toujours bienveillantes et la plupart du
temps encourageantes et constructives, m�me en cas de rejet.

Il n'y a donc pas de raison de ne pas soumettre aux JFLA !

Orateurs invit�s
----------------
 Pierre Weis (INRIA).
 C�dric Fournet (Microsoft Research).


Cours
-----
 Yves Bertot (INRIA).
 Renaud Rioboo (Lip6).

Comit� de programme
-------------------
        Sandrine Blazy, Pr�sident (CEDRIC, ENSIIE, �vry)

        Alan Schmitt, Vice-Pr�sident (INRIA Grenoble - Rh�ne-Alpes)

        Horatiu Cirstea (INRIA Nancy - Grand Est)

        Tom Hirschowitz (LIP, ENS Lyon)

        Mathieu Jaume (LIP6, Universit� Paris 6)

        Delia Kesner (PPS, Universit� Paris 7)

        Nicolas Magaud (LSIIT, Universit� Louis Pasteur, Strasbourg)

        Marc Pouzet (LRI, Universit� Paris-Sud)

        Laurence Rideau (INRIA Sophia Antipolis - M�diterran�e)

        Francesco Zappa Nardelli (INRIA Paris - Rocquencourt)

Soumission
----------
Date limite de soumission : 16 octobre 2007

Les soumissions doivent �tre soit r�dig�es en fran�ais, soit
pr�sent�es en fran�ais. Elles sont limit�es � 15 pages A4. Le style
latex est impos� et se trouve sur le site WEB des journ�es � l'adresse
suivante :

         http://jfla.inria.fr/2008/actes.sty

La soumission est uniquement �lectronique, selon la m�thode d�taill�e dans

         http://jfla.inria.fr/2008/instructions-fra.html

Les soumissions sont � envoyer � la pr�sidente du comit� de programme,
avec pour titre de votre message ``SOUMISSION JFLA 2008'', � l'adresse
suivante :

             jfla2008@ensiie.fr

Les intentions de soumission envoy�es le plus t�t possible � l'adresse
ci-dessus seront les bienvenues.


Dates importantes
-----------------
16 octobre 2007 : Date limite de soumission
16 novembre 2007 : Notification aux auteurs
10 d�cembre 2007 : Remise des articles d�finitifs
14 janvier 2008 : Date limite d'inscription aux journ�es
26 au 29 janvier 2008 : Journ�es

Pour tout renseignement, contacter
----------------------------------
Ga�lle Dorkeld
INRIA Rocquencourt
Bureau des Cours et Colloques (JFLA2003)
Domaine de Voluceau - BP 105
78153 Le Chesnay Cedex
T�l.: +33 (0) 1 39 63 56 00 - Fax : +33 (0) 1 39 63 56 38
email : Gaelle.Dorkeld@inria.fr

http://jfla.inria.fr/2008/
