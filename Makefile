.phony: help
# Use awk to parse the Makefile and generate a help menu.
# Add `## your description` after a `target:` to make it appear in the help menu.
help: ## Montre l'aide en ligne
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "%-20s%s\n", $$1, $$2}' $(MAKEFILE_LIST)

.phony: rcopy

# Information sur la publication distante
REMOTE_USER ?= bonichon
REMOTE ?= yquem.inria.fr
REMOTE_DIR ?= /net/yquem/infosystems/www/jfla

SCP_URL = $(REMOTE_USER)@$(REMOTE):$(REMOTE_DIR)

# Répertoire où réside le site local
LOCAL = _site

rsync: ## Utilisation de rsync pour pousser les changements (KO)
	rsync -avz $(LOCAL)/ $(SCP_URL)

rcopy: ## Utilisation de scp pour pousser les changements
	scp -r $(LOCAL) $(SCP_URL)


# Définitions pour une copie minimale du site permettant de mettre à jour la
# conférence de l'année choisie seulement

# L'année de la conférence
YEAR ?= 2021

# La liste des suffixes fichiers/répertoires à copier
MINIMAL_FILES ?= \
	assets \
	jfla$(YEAR).html \
	static/img

minimal: ## Copie minimale par scp des fichiers de l'année en cours (YEAR)
minimal: $(MINIMAL_FILES:%=_site/%)
	for f in $(MINIMAL_FILES); do \
		scp -r $(LOCAL)/$$f $(SCP_URL)/`dirname $$f`; \
	done
