{ pkgs ? (import <nixpkgs> { }) }:
let jekyll_env = with pkgs; bundlerEnv rec {
  name = "jekyll_env";
  inherit ruby;
  gemfile = ./Gemfile;
  lockfile = ./Gemfile.lock;
  gemset = ./gemset.nix;
};
in
{
  jekyll_serve =
    pkgs.stdenv.mkDerivation rec {
      name = "jfla.gs";
      buildInputs = with pkgs; [ jekyll_env bundler ruby nodejs ];

      shellHook = ''
        # exec ${jekyll_env}/bin/jekyll serve --watch
      '';
    };
}
